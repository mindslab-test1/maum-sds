CREATE TABLE `Chatbot`.`Account` (
  `No` int(5) NOT NULL AUTO_INCREMENT,
  `Name` varchar(400) DEFAULT NULL,
  `Email` nvarchar(400) DEFAULT NULL,
  `Host`  nvarchar(400) DEFAULT NULL,
  `Company` nvarchar(400) DEFAULT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
ALTER TABLE `Chatbot`.`Account` AUTO_INCREMENT = 1;

CREATE TABLE `Chatbot`.`Intent` (
  `No` int(5) NOT NULL AUTO_INCREMENT,
  `Language` int(1) DEFAULT NULL,
  `Main` varchar(2000) DEFAULT NULL,
  `Name` varchar(400) DEFAULT NULL,
  `Entity` varchar(100) NOT NULL,
  `Answer` int(5) DEFAULT NULL,
  `Next` varchar(500) DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  `Description` varchar(2000) DEFAULT NULL,
  `Account` int DEFAULT NULL,
  `greetyn` char(1) DEFAULT NULL,
  `greet` int DEFAULT NULL,
  `TmpNo` int DEFAULT NULL,
  `TmpNextIntent` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
ALTER TABLE `Chatbot`.`Intent` AUTO_INCREMENT = 1;

CREATE TABLE `Chatbot`.`Entities` (
  `No` int(5) NOT NULL AUTO_INCREMENT,
  `Intent` int(5) DEFAULT NULL,
  `Name` varchar(400) DEFAULT NULL,
  `Val` varchar(400) DEFAULT NULL,
  `Answer` int(5) DEFAULT NULL,
  `Account` int DEFAULT NULL,
  `Language` int(1) DEFAULT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
ALTER TABLE Chatbot. Entities AUTO_INCREMENT = 1;

CREATE TABLE `Chatbot`.`Answer` (
  `No` int(5) NOT NULL AUTO_INCREMENT,
  `Answer` mediumtext DEFAULT NULL,
  `Account` int DEFAULT NULL,
  `TmpNo` int DEFAULT NULL,
  `Language` int(1) DEFAULT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
ALTER TABLE `Chatbot`.`Answer` AUTO_INCREMENT = 1;

CREATE TABLE `Chatbot`.`BackendInfo` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `host` varchar(10) DEFAULT NULL,
  `service` varchar(10) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
ALTER TABLE `Chatbot`.`BackendInfo` AUTO_INCREMENT = 1;

CREATE TABLE BertIntent (
	No int auto_increment primary key,
	Name nvarchar(100),
	Language int,
	BertItfId int
);

CREATE TABLE Fallback (
	Id int auto_increment primary key,
	Host int,
	Intent int,
	Language int
);

CREATE TABLE IntentRel (
	SrcIntent int,
	BertIntent int,
	DestIntent int,
	ConditionAnswer int
);

DROP PROCEDURE IF EXISTS `Chatbot`.`SP_INSERT_SPLITED`;

DELIMITER $$
$$
CREATE PROCEDURE `Chatbot`.`SP_INSERT_SPLITED`(
	string VARCHAR(500),
	delimiter CHAR(8),
	tableName VARCHAR(64))
BEGIN
	SET @idx = 0;
	SET @valueList = '';
	SET @occurrences = LENGTH(string) - LENGTH(REPLACE(string, delimiter, ''));
	splitLoop: WHILE @occurrences > 0 DO
		SET @idx = @idx + 1;
		SET @splited = SUBSTRING_INDEX(string, delimiter, 1);
		SET @valueList = concat(@valueList,IF(@idx=1,'',','),'(',@idx,',\'',@splited,'\')');
		SET @occurrences = LENGTH(string) - LENGTH(REPLACE(string, delimiter, ''));
		IF @occurrences = 0 THEN
		    LEAVE splitLoop;
		END IF;
		SET string = SUBSTRING(string,CHAR_LENGTH(@splited)+2);
	END WHILE;
	IF @idx > 0 THEN
		set @stmtText = concat('INSERT INTO ',tableName,'(no,val) VALUES ',@valueList);
		PREPARE stmt FROM @stmtText;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
	END IF;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS SP_GET_RESPONSE;

DELIMITER $$
$$
create procedure SP_GET_RESPONSE(
    account varchar(500),
    session varchar(500),
    intent varchar(500),
    entityNameCs varchar(500),
    entityValuesCs varchar(500),
    lang int(8))
BEGIN
        SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        SET @lang = lang;
        SET @account = account;
        SET @si = session;
        SET @it = intent;
        SET @en = entityNameCs;
        SET @ev = entityValuesCs;

        SET @in = '';
        SET @ite = 'N';
        SET @an = '';

        SELECT i.`No`, i.`Entity`, i.`Answer`
            INTO @in, @ite, @an
            FROM fast_aicc.Intent i
                WHERE i.`Main` = @it AND i.`Account` = @account AND i.`Language` = @lang;

        IF @ite <> 'N' THEN
            DROP TABLE IF EXISTS enTemp, evTemp, iteTemp, entityTemp;

            CREATE TEMPORARY TABLE enTemp (
                no int(8) NOT NULL,
                val varchar(20) DEFAULT NULL
            ) ENGINE = memory;
            CREATE TEMPORARY TABLE evTemp LIKE enTemp;
            CREATE TEMPORARY TABLE iteTemp LIKE enTemp;

            CALL SP_INSERT_SPLITED('', ',', 'enTemp');
            CALL SP_INSERT_SPLITED('', ',', 'evTemp');

            CREATE TEMPORARY TABLE entityTemp (
                en varchar(1000),
                ev varchar(1000)
            ) ENGINE = memory;

            INSERT INTO entityTemp(en, ev)
                SELECT c.val, d.val
                FROM enTemp c INNER JOIN evTemp d ON c.no = d.no;

            CALL SP_INSERT_SPLITED(@ite, ',', 'iteTemp');
            SET @ic = (SELECT COUNT(*) FROM iteTemp);
            SET @ic2 = (SELECT COUNT(*)
                FROM Entities a INNER JOIN entityTemp f ON a.Name = f.en
                WHERE a.Intent = @in
                    AND a.`No` IN (SELECT val FROM iteTemp)
                    AND a.Val = CASE
                            WHEN a.Val = '*' AND f.ev NOT IN ('', null) THEN '*'
                            ELSE f.ev
                        END
                    AND a.Account = @account
                    AND a.`Language` = @lang);
            SET @ic3 = (SELECT COUNT(DISTINCT a.`No`)
                FROM Entities a INNER JOIN Answer b ON a.Answer = b.`No`
                WHERE a.Intent = @in
                    AND a.Name NOT IN (select en FROM entityTemp)
                    AND a.Val NOT IN (select ev FROM entityTemp)
                    AND a.Val = '*'
                    AND a.Account = @account
                    AND a.`Language` = @lang);
            IF @ic > @ic2 AND @ic3 <> 0 THEN
                SELECT b.Answer
                FROM Entities a INNER JOIN Answer b ON a.Answer = b.`No`
                    WHERE a.Intent = @in
                        AND a.Name NOT IN (SELECT en FROM entityTemp)
                        AND a.Val NOT IN (SELECT ev FROM entityTemp)
                        AND a.Account = @account
                        AND a.`Language` = @lang;
            ELSE
                SET @if = 2;
            END IF;
            DROP TABLE enTemp, evTemp, iteTemp, entityTemp;
        ELSE
            SET @if = 2;
        END IF;

        IF @if = 2 THEN
            SET @ni2 = (SELECT ifnull(i.`Next` ,'') FROM fast_aicc.Intent i WHERE i.`No` = @in AND i.Account = @account);

            DROP TABLE IF EXISTS ni2Temp;
            CREATE TEMPORARY TABLE ni2Temp (
                no int(8),
                val varchar(20),
                intent int(10),
                type char(20)
            ) ENGINE = memory;
            CALL SP_INSERT_SPLITED(@ni2, ',', 'ni2Temp');
            UPDATE ni2Temp SET
                intent = CONVERT(IF(val REGEXP '^[0-9]+$', val, SUBSTRING(val, 2)), UNSIGNED),
                type = IF(val REGEXP '^[0-9]+$', 'L', LEFT(val, 1));

            SELECT
            DISTINCT
                c.Answer AS hi
                , b.Answer AS answer
                , '' AS `no`
                , '' AS `next`
                , '' AS name
                , '' AS `type`
                , '' AS url
                , '' AS ds
            FROM Intent a INNER JOIN Answer b ON a.Answer = b.`No`
                LEFT OUTER JOIN Answer c ON a.greet = c.`No`
            WHERE b.`No` = @an
                AND a.Account = @account
                AND a.`Language` = @lang
            UNION ALL
            SELECT DISTINCT
                '',
                '',
                d.`No`,
                d.Main AS `next`,
                d.Name,
                CASE
                    WHEN LEFT(e.val,1) = 'b' THEN 'B'
                    WHEN LEFT(e.val,1) = 'i' THEN 'I'
                    ELSE 'L'
                END AS `type`,
                d.URL AS url,
                d.Description AS ds
            FROM Intent d INNER JOIN ni2Temp e ON d.`No` = e.intent
                WHERE d.Account = @account AND d.`Language` = @lang;
            DROP TABLE ni2Temp;
        END IF;

        SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
END$$
DELIMITER ;


