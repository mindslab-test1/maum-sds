-- Create user
create user sa@localhost identified by 'Mindslab!1';

-- Create Chatbot Database for Collector & Maker
CREATE DATABASE Chatbot DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON Chatbot.* TO sa@localhost IDENTIFIED BY 'Mindslab!1';
FLUSH PRIVILEGES;

DROP TABLES Account, Intent, Entities, Answer, BackendInfo;

-- Create ChatbotLog Database for Logging
CREATE DATABASE ChatbotLogDev DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON ChatbotLogDev.* TO sa@localhost IDENTIFIED BY 'Mindslab!1';
FLUSH PRIVILEGES;

DROP TABLES AnswerLog, DebugLog, EntityDetailLog, EntityLog, IntentLog, StatsLog;
