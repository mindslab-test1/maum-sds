CREATE TABLE `ChatbotLogDev`.`AnswerLog` (
  `id` int NOT NULL,
  `host` int DEFAULT NULL,
  `answer` mediumtext DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `utter` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ChatbotLogDev`.`DebugLog` (
  `id` int NOT NULL,
  `session` varchar(50) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `message` varchar(512) DEFAULT NULL,
  `host` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ChatbotLogDev`.`EntityDetailLog` (
  `id` int NOT NULL,
  `entityLogId` int DEFAULT NULL,
  `entityName` varchar(100) DEFAULT NULL,
  `entityValue` varchar(100) DEFAULT NULL,
  `host` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ChatbotLogDev`.`EntityLog` (
  `id` int NOT NULL,
  `utter` varchar(100) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `host` int DEFAULT NULL,
  `session` varchar(50) DEFAULT NULL,
  `lang` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ChatbotLogDev`.`IntentLog` (
  `id` int NOT NULL,
  `utter` varchar(400) DEFAULT NULL,
  `intent` varchar(100) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `host` int DEFAULT NULL,
  `session` varchar(50) DEFAULT NULL,
  `lang` int DEFAULT NULL,
  `prob` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `StatsLog` (
  `id` int NOT NULL,
  `host` int DEFAULT NULL,
  `session` varchar(20) DEFAULT NULL,
  `utter` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
