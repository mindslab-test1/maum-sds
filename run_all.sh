#!/usr/bin/env bash

if [ "0" == "$#" ]; then
    export SDS_PROFILE="dev"
    export SDS_PROFILE_OFFSET="20"
else
    export SDS_PROFILE="$1"
    export SDS_PROFILE_OFFSET="$2"
fi

echo "sds profile ${SDS_PROFILE}, port offset ${SDS_PROFILE_OFFSET}"

let "frontend_port = 10000 + ${SDS_PROFILE_OFFSET}"
java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 -Dserver.port=$frontend_port \
 ./brain-sds-frontend/target/brain-sds.jar >> sds_fronted.log &

let "adapter_port = 7641 + ${SDS_PROFILE_OFFSET}"
java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 -Dserver.port=$adapter_port \
 ./brain-sds-adapter/target/brain-sds.jar >> sds_adapter.log &

let "collector_port = 6941 + ${SDS_PROFILE_OFFSET}"
java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 -Dserver.port=$collector_port \
 ./brain-sds-collector/target/brain-sds.jar >> sds_collector.log &

let "maker_port = 7650 + ${SDS_PROFILE_OFFSET}"
java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 -Dserver.port=$maker_port \
 ./brain-sds-maker/target/brain-sds.jar >> sds_maker.log &

let "logger_port = 6950 + ${SDS_PROFILE_OFFSET}"
java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 -Dserver.port=$logger_port \
 ./brain-sds-log-store/target/brain-sds.jar >> sds_logger.log &

let "cache_server_port = 11160 + ${SDS_PROFILE_OFFSET}"
java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 -Dserver.port=$logger_port \
 ./brain-sds-cache/target/brain-sds.jar >> sds_cache_server.log &

python brain-sds-memory/sds_memory.py --profile=${SDS_PROFILE} --port_offset=${SDS_PROFILE_OFFSET} &

echo "maum SDS up and running, profile: ${SDS_PROFILE}, port offset: ${SDS_PROFILE_OFFSET}"
