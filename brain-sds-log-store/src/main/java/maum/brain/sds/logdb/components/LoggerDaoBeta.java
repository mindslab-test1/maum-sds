package maum.brain.sds.logdb.components;

import java.util.Map;
import maum.brain.sds.data.dto.log.SdsLogStatsRequest;
import maum.brain.sds.logdb.data.*;
import maum.brain.sds.util.logger.LogSessionCounselorRequest;
import maum.brain.sds.util.logger.SdsEntity;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LoggerDaoBeta {
    private static final Logger logger = LoggerFactory.getLogger(LoggerDaoBeta.class);

    public int logIntent(String utter, String intent){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        return mapper.logIntent(
            new LogIntentDto(
                utter, intent
            )
        );
    }

    public int logIntent(String utter, String intent, int host){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        return mapper.logIntentHost(
            new LogIntentDto(utter, intent, host)
        );
    }

    public int logIntent(LogIntentDto dto){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        try{
            if(dto.getAnswer()==null || dto.getAnswer().length()==0){
                dto.setAnswer(mapper.selectAnswer(dto));
            }
        }catch (Exception e){
            System.out.println("dto setAnswer Error : " + dto.toString());
        }
        return mapper.logIntentWProb(dto);
    }

    public int logEntity(String utter, List<SdsEntity> entities){
        SqlSession session = LoggingSqlConfig.getNewSqlSessionInstance();

        try {
            LoggerMapper mapper = session.getMapper(LoggerMapper.class);
            LogEntityDto dto = new LogEntityDto(utter);
            mapper.logEntity(dto);

            int id = dto.getId();
            System.out.println(id);

            for (SdsEntity entity: entities)
                mapper.logEntityDetail(
                    new LogEntityDetailDto(
                        id, entity.getEntityName(), entity.getEntityValue()
                    )
                );

            session.commit();

            return 0;
        } finally {
            LoggingSqlConfig.closeSession(session);
        }
    }

    public int logEntity(String utter, List<SdsEntity> entities, int host){
        try(SqlSession session = LoggingSqlConfig.getNewSqlSessionInstance()){
            LoggerMapper mapper = session.getMapper(LoggerMapper.class);
            LogEntityDto dto = new LogEntityDto(utter, host);
            mapper.logEntity(dto);

            int id = dto.getId();
            for (SdsEntity entity: entities)
                mapper.logEntityDetailHost(
                    new LogEntityDetailDto(
                        id, entity.getEntityName(), entity.getEntityValue(), host
                    )
                );

            session.commit();

            return 0;
        }
    }

    public int logEntity(LogEntityDto entityDto, List<SdsEntity> entities){
        try(SqlSession session = LoggingSqlConfig.getNewSqlSessionInstance()){
            LoggerMapper mapper = session.getMapper(LoggerMapper.class);
            LogEntityDto dto = entityDto;
            mapper.logEntityHostSession(dto);

            int id = dto.getId();
            for (SdsEntity entity: entities)
                mapper.logEntityDetailHost(
                    new LogEntityDetailDto(
                        id, entity.getEntityName(), entity.getEntityValue(), dto.getHost()
                    )
                );

            session.commit();

            return 0;
        }
    }

    public int logAnswer(String utter, String answer){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        return mapper.logAnswer(
            new LogAnswerDto(
                utter, answer
            )
        );
    }

    public int logAnswer(String utter, String answer, int host){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        return mapper.logAnswerHost(
            new LogAnswerDto(utter, answer, host)
        );
    }

    public void logMessage(String level, String msg){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        mapper.logMessage(
            new LogMessageDto(
                level, msg
            )
        );
    }

    public void logStats(SdsLogStatsRequest request){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        mapper.logStats(request);
    }

    public Map<String, Object> sessionLogCheck(String input){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        return mapper.sessionLogCheck(input);
    }

    public void sessionLogCountUpdate(LogSessionCntDto logSessionCntDto){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        mapper.sessionLogCountUpdate(logSessionCntDto);
    }

    public void sessionLogCountInsert(LogSessionDto logSessionDto){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        mapper.sessionLogCountInsert(logSessionDto);
    }

    public int sessionMaxIDCheck(){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        return mapper.sessionMaxIDCheck();
    }

    public void updateCounselor(LogSessionCounselorRequest logSessionCounselorRequest){
        LoggerMapper mapper = LoggingSqlConfig.getSqlSessionInstance().getMapper(LoggerMapper.class);
        mapper.updateCounselor(logSessionCounselorRequest);
    }
}
