#!/usr/bin/env bash
export SDS_PROFILE="production"
export SDS_PROFILE_OFFSET="0"
echo "sds profile ${SDS_PROFILE}, port offset ${SDS_PROFILE_OFFSET}"

java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 -Dserver.port=443 \
 ./brain-sds-frontend/target/brain-sds.jar >> sds_fronted.log &
java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 ./brain-sds-adapter/target/brain-sds.jar >> sds_adapter.log &
java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 ./brain-sds-collector/target/brain-sds.jar >> sds_collector.log &
java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 ./brain-sds-maker/target/brain-sds.jar >> sds_maker.log &
java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 ./brain-sds-log-store/target/brain-sds.jar >> sds_logger.log &
 java -jar \
 -Dbrain.sds.profile=${SDS_PROFILE} -Dbrain.sds.port.offset=${SDS_PROFILE_OFFSET} \
 ./brain-sds-cache/target/brain-sds.jar >> sds_cache_server.log &
python brain-sds-memory/sds_memory.py --profile=${SDS_PROFILE} --port_offset=${SDS_PROFILE_OFFSET} &

echo "maum SDS up and running, port offset ${SDS_PROFILE_OFFSET}"
