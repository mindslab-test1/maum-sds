package first.builder.vo;

public class AnswerVO {

  private String answer;
  private String account;
  private String language;
  private int num;
  private String serverUrl;


  public String getAnswer() {
    return answer;
  }

  public String getServerUrl() {
    return serverUrl;
  }

  public String getAccount() {
    return account;
  }

  public int getNum() {
    return num;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public void setNum(int num) {
    this.num = num;
  }

  public void setServerUrl(String serverUrl) {
    this.serverUrl = serverUrl;
  }
}
