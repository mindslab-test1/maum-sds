<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">

    <!-- cache 지우는 meta 태그 -->
    <meta http-equiv="Cache-Control" content="no-cache" /> <!-- 캐시가 되지 않도록 정의 -->
    <meta http-equiv="Pragma" content="no-cache" /> <!-- 캐시가 되지 않도록 정의 -->
    <meta http-equiv="Expires" content="-1"> <!-- 즉시 캐시만료 -->
    <title>chatbot builder Intention</title>
    <script src="${pageContext.request.contextPath}/js/builderContents/chatbotBuilderIntention.js?v=${queryString}"></script>
</head>
<body>
<div class="srchArea">
    <div class="fr iptBox">
        <!-- [D] 인풋에서 엔터 키 입력 시 btn_search가 클릭됩니다 -->
        <input type="text" class="ipt_txt search" name="intentText" autocomplete="off">
        <button type="button" class="btn_search" id="searchIntent"><span class="text_hide">검색하기</span></button>
    </div>
</div>

<div class="tbl_box_info">
    <div class="fl">
        <p class="path"><em>의도</em></p>
    </div>
    <div class="fr">
        <a href="#chat_learning_management" id="chatbot_learning" class="btn_secondary btn_lyr_open popper_box" style="display: none" onclick="handleLyr()">챗봇 학습
<%--            [D] 문장에 따라 width를 다르게 지정해 주어야 합니다. --%>
            <span class="popper bg_bk" id="alert_nqa_train_start" style="width: 160px; display: none">학습되지 않은 데이터가 있습니다. 학습을 시작해 주세요.</span>
            <span class="popper bg_bk" id="alert_nqa_training" style="width: 85px; display: none">학습중 입니다.</span>
        </a>
        <a href="#chat_intention_add" class="btn_secondary btn_lyr_open el_intent_step01" onclick="addPage()">추가</a>
    </div>
</div>

<div class="tbl_wrap scroll">
    <table class="tbl_box_lst" summary="순번, 의도, 정규식 문장, NQA 문장, BERT 문장, 상세, 삭제">
        <caption class="hide">순번, 의도, 정규식 문장, NQA 문장, BERT 문장, 상세, 삭제</caption>
        <colgroup id="intentColgroup">
            <col style="width: 65px;"><col style="width: 15%;"><col><col>
            <col style="width: 85px;"><col style="width: 85px;">
        </colgroup>
        <thead id="intentHead">
        <tr>
            <th>순번</th>
            <th>의도</th>
            <th>정규식 문장</th>
            <th>학습 문장</th>
            <%--<th>BERT 학습 문장</th>--%>
            <th><span class="text_hide">상세</span></th>
            <th><span class="text_hide">삭제</span></th>
        </tr>
        </thead>
        <tbody id="intentBody">
        </tbody>

<%--        [D] 정규식 컬럼 추가--%>
        <tbody id="regexBody">
        </tbody>
    </table>

    <div class="pagination">
        <button type="button" class="first" href="javascript:goPage(1)"><span>&laquo;</span></button>
        <button type="button" class="prev" href="javascript:goPage('${paging.prevPage}')"><span>&lsaquo;</span></button>
        <div class="pages">
            <c:forEach begin="${paging.pageRangeStart}" end="${paging.pageRangeEnd}" varStatus="loopIdx">
                <c:choose>
                    <c:when test="${paging.currentPage eq loopIdx.index}">
                        <span class="page active">${loopIdx.index}</span>
                    </c:when>
                    <c:otherwise>
                        <span class="page" href="javascript:goPage('${loopIdx.index}')">${loopIdx.index}</span>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </div>
        <button type="button" class="next" href="javascript:goPage('${paging.nextPage}')"><span>&rsaquo;</span></button>
        <button type="button" class="last" href="javascript:goPage('${paging.totalPage}')"><span>&raquo;</span></button>
    </div>
</div>
<input type="hidden" id="currentPage" value="1">
<input type="hidden" id="searchIntentText">
</body>
</html>
