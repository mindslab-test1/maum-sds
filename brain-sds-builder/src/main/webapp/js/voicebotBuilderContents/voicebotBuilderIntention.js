var intentTable = null;
var intentStcList = null;

$(document).ready(function(){

  if ( scenario ) {
    intentData.lang = data.lang;
    intentData.chatName = function(){
      var name = data.name;
      if (data.lang === 1) {//한국어
        name.splice(' (한국어)', '');
      } else {//영어
        name.splice(' (영어)', '');
      }
      return name;
    }
    intentData.host = data.host;

    if ( !intentTableData.length ) {
      getIntention(intentData).then(function(){
        drawTable();
      });
    } else {
      drawTable();
    }
  }
});

function getIntention(obj) {
  $('#vb_wrap').addClass('loading');

  //getIntention
  return $.ajax({
    url: "voiceBot/getIntention",
    data: JSON.stringify(obj),
    type: "POST",
    contentType: 'application/json'

  }).then(function (res) {
    intentStcList = res.intentStcList;

    for (var i = 0; i < intentStcList.length; i++) {
      var obj = {};
      obj.commonIntent = intentStcList[i].Task === '0' || intentStcList[i].Task === 0 ? 'O' : 'X';
      obj.label = intentStcList[i].Name === '' ? 'always' : intentStcList[i].Name;
      obj.regex = intentStcList[i].regex ? intentStcList[i].regex : '';
      obj.nqa = intentStcList[i].question ? intentStcList[i].question : '';
      obj.delete = obj.label;

      intentTableData.push(obj);
    }

    $('#vb_wrap').removeClass('loading');

  }).catch(function () {
    mui.alert('페이지를 불러오지 못했습니다.<br> 다시 시도해주세요.');
    $('#vb_wrap').removeClass('loading');
    console.log('[getIntention error]');
  });
}

function deleteIntention(intent) {
  mui.confirm(intent + ' 을(를) 삭제하시겠습니까?', {
    onClose: function(isOk){
      if (isOk) {
        $('#vb_wrap').addClass('loading');

        intentData.bertIntentName = intent;
        intentData.langList = [data.lang];

        intentDelete(intentData); //db 삭제 - 시나리오 제이슨 저장 의도 부분 고도화 되면 없어질 코드
        intentDeleteScenario(intentData) //시나리오 제이슨 수정/삭제
          .then(function(){intentSaveScenario() //시나리오 제이슨 저장
            .then(function() {updateIntentTable(intentTableData); //의도 table update
          });
        });
      }
    }
  });

  //db 삭제 - 시나리오 제이슨 저장 의도 부분 고도화 되면 없어질 코드
  function intentDelete(obj) {
    return $.ajax({
      url : "voiceBot/deleteIntent",
      data : JSON.stringify(obj),
      type: "POST",
      contentType: 'application/json'
    }).catch(function() {
      mui.alert(intent + ' 삭제를 실패하였습니다.<br> 다시 시도해주세요.');
      $('#vb_wrap').removeClass('loading');
      console.log('[intentDelete error]');
    });
  }

  function intentDeleteScenario(obj) {
    return $.ajax({
      url : "voiceBot/selectIntent",
      data : JSON.stringify(obj),
      type: "POST",
      contentType: 'application/json'
    }).then(function(res) {
      var connectIntent = res;
      allTaskInfo = allTask();

      for (var i=0; i<connectIntent.length; i++) {
        var srcName = connectIntent[i].srcName;
        var destName = connectIntent[i].destName;
        var src = taskInfo.find(e => e.label === srcName);
        var dest = taskInfo.find(e => e.label === destName);

        deleteScenarioIntent(src.id, dest.id, dest.label, intent);
      }

      var dataIndex = intentTableData.findIndex(e => e.label === intent);
      intentTableData.splice(dataIndex, 1);

    }).catch(function() {
      mui.alert(intent + ' 삭제를 실패하였습니다.<br> 다시 시도해주세요.');
      $('#vb_wrap').removeClass('loading');
      console.log('[deleteIntention error]');
    });
  }

  function intentSaveScenario() {
    return $.ajax({
      url: "simpleBot/applyV2",
      data: JSON.stringify({
        simplebotId: Number(data.simplebotId),
        userId: data.userId,
        companyId: data.companyId,
        scenarioJson: JSON.stringify(scenario)
      }),
      type: "POST",
      contentType: 'application/json'

    }).then(function () {
      mui.alert(intent + ' 을(를) 삭제하였습니다.');
      updateIntentTable(intentTableData);
      $('#vb_wrap').removeClass('loading');

    }).catch(function () {
      $('#vb_wrap').removeClass('loading');
      mui.alert(intent + ' 삭제를 실패하였습니다.<br> 다시 시도해주세요.');
      console.log('[intentSaveScenario error]');
    });
  }
}

function deleteScenarioIntent(sourceId, targetId, target, name) {
  // 노드(intentList) 찾기
  var nodeIndex = scenario.nodes.findIndex(e => e.id === sourceId);
  var intentIndex = scenario.nodes[nodeIndex].attr[0].intentList.findIndex(e => e.intent === name && e.nextTask === target);

  if (intentIndex > -1) { //task와 연결된 intent를 찾음
    if (targetId === '종료') {//종료 노드 id 찾기
      var endTasks = scenario.nodes.filter(e => e.type === 'end');

      for (var i=0; i<endTasks.length; i++) {
        var connectedEnd = scenario.edges.find(e => e.source === sourceId && e.target === endTasks[i].id)

        if (connectedEnd) {
          targetId = connectedEnd.target;
          break;
        }
      }
    }

    //엣지, 노드(intentList) 제거
    var edgeIndex = scenario.edges.findIndex(e => e.source === sourceId && e.target === targetId);
    scenario.edges.splice(edgeIndex, 1);
    scenario.nodes[nodeIndex].attr[0].intentList.splice(intentIndex, 1);

    //다음태스크가 다른 edge에 연결되어 있는지 확인
    var targetIndex = scenario.edges.findIndex(e => e.target === targetId);

    if ( targetIndex === -1 && target === '종료' ) {//다른 edge에 연결되어 있지 않고 종료면 삭제
      var nodeIdx = scenario.nodes.findIndex(e => e.id === targetId);
      scenario.nodes.splice(nodeIdx, 1);
    }

    //엣지 수정
    var labelFilter = scenario.nodes[nodeIndex].attr[0].intentList.filter(e => e.nextTask === target);
    var labelArr = labelFilter.map(e => e.intent === '' ? 'always' : e.intent);
    var label = labelArr.join('/');
    var edgeArr = scenario.edges.filter(e => e.source === sourceId && e.target === targetId);

    edgeArr.forEach(function(e) {
      e.data.label = label;
    });

  } else {
    console.log('[deleteScenarioIntent error]');
  }
}

function drawTable() {
  intentTable = $('#table_intent table').DataTable({
    "language": {
      "emptyTable": "등록된 데이터가 없습니다.",
      "lengthMenu": "페이지당 _MENU_ 개씩 보기",
      "info": "현재 _START_ - _END_ / _TOTAL_건",
      "infoEmpty": "데이터 없음",
      "infoFiltered": "( _MAX_건의 데이터에서 필터링됨 )",
      "search": "",
      "zeroRecords": "일치하는 데이터가 없습니다.",
      "paginate": {
        "next": "›",
        "previous": "‹",
        "first" : "«",
        "last" :"»",
      }
    },
    bFilter:false,
    bInfo:false,
    sDom:"lrtip",
    lengthChange : false,
    fixedColumns: true,
    autoWidth: false, //css width
    searching: true, //검색
    ordering : false, //정렬
    paging: true, // paging 디폴트 : row 10개, 페이징 5개
    pagingType : "full_numbers_no_ellipses",
    data: intentTableData, //참조 data
    columns: [
      {
        data: "commonIntent",
        name: 'commonIntent',
        title: '공통 여부',
        searchable: false,
        width: '80px',
      },
      {
        data: "label",
        name: 'label',
        title: 'INTENT',
        searchable: true,
        render: function(data){
          var dropType = 'name';
          return "<a onclick='moveLinkDetail(\""+dropType+"\");'>"+data+"</a>";
        }
      },
      {
        data: "regex",
        title: '정규표현식',
        searchable: true,
        render: function(data){
          var dropType = 'regex';
          return "<a onclick='moveLinkDetail(\""+dropType+"\");'>"+data+"</a>";
        }
      },
      {
        data: "nqa",
        title: '학습문장',
        searchable: true,
        render: function(data){
          var dropType = 'nqa';
          return "<a onclick='moveLinkDetail(\""+dropType+"\");'>"+data+"</a>";
        }
      },
      {
        data: "delete",
        title: '',
        searchable: false,
        width: '90px',
        render: function(data){
          return "<button type='button' class='btn_line_warning' onclick='deleteIntention(\""+data+"\");'>삭제</button>"
        },
      },
    ],
  });
  intentTable.draw();

  // dataTable 통합 검색
  $("#intent_search").keyup(function(){
    intentTable.search($(this).val()).draw();
  });
}

function updateIntentTable(data) {
  intentTable.clear();
  intentTable.rows.add(data).draw();
}

function moveLinkDetail(dropType){ //dropType : name, regex, nqa
  var $target = $(event.target);
  var thisData = intentTable.row($target.parents('tr')).data();

  moveIntentDetail(data.host, thisData.label, dropType);
}