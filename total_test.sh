UTTER_URL="http://13.125.179.153:6941/collect/run/utter"
INTENT_URL="http://13.125.179.153:6941/collect/run/intent"

json_utter_data(){
cat <<EOF
{
 "host": $host,
 "session": "test",
 "data": {
          "utter": "처음으로"
 },
 "entities": [
        {
        "entityName": "cust_name",
        "entityValue": "홍길동"
        },
        {
        "entityName": "cust_age",
        "entityValue": "25"
        }
 ],
 "lang": "1"
}
EOF
}
json_intent_data(){
cat <<EOF
{
 "host": $host,
 "session": "test",
 "data": {
          "intent": "처음으로"
 },
 "entities": [
        {
        "entityName": "cust_name",
        "entityValue": "홍길동"
        },
        {
        "entityName": "cust_age",
        "entityValue": "25"
        }
 ],
 "lang": "1"
}
EOF
}

hotelNm_array=( "메이필드" "호텔브라운스위트서울" "호텔해담채" "호텔라비타" "로사나" "호텔서울로얄" "호텔소울하다" "스위트호텔제주" "스위트호텔남원" "스위트호텔낙산" "스위트호텔경주" "웨스턴그레이스" "골든튤립" "삼성라인" "라이프치과" "메이린클릭닉 압구정점" "파라독스펜션" "헬로피스펜션" "라마다오텔앤스위트서울남대문" "브라운스위트제주" "파티오세븐호텔" "코리아나호텔" "라마다바이원덤제주함덕" "그라벨호텔" "리센오션파크속초" "레드타이 영업용 챗봇" "동아출판 초등" "동아출판 중고등" "마인즈랩" )
host_array=( "5" "69" "41" "53" "18" "19" "52" "34" "35" "36" "37" "16" "2" "51" "4" "94" "124" "110" "119" "104" "125" "161" "217" "222" "290" "132" "186" "133" "408" )

for (( i=0; i<${#host_array[@]}; i++)); do
	
host=${host_array[$i]}

#HTTP_RESPONSE=$(curl -sS --write-out "HTTPSTATUS:%{http_code}" -H "Content-Type: application/json" -X POST -d "$(json_data)" $value)

HTTP_UTTER_RESPONSE=$(curl -sS --write-out "HTTPUTTERSTATUS:%{http_code}" -H "Content-Type: application/json" -X POST -d "$(json_utter_data)" $UTTER_URL)

HTTP_UTTER_BODY=$(echo $HTTP_UTTER_RESPONSE | sed -e 's/HTTPUTTERSTATUS\:.*//g')

HTTP_UTTER_STATUS=$(echo $HTTP_UTTER_RESPONSE | tr -d '\n' | sed -e 's/.*HTTPUTTERSTATUS://')

HTTP_INTENT_RESPONSE=$(curl -sS --write-out "HTTPINTENTSTATUS:%{http_code}" -H "Content-Type: application/json" -X POST -d "$(json_intent_data)" $INTENT_URL)

HTTP_INTENT_BODY=$(echo $HTTP_INTENT_RESPONSE | sed -e 's/HTTPINTENTSTATUS\:.*//g')

HTTP_INTENT_STATUS=$(echo $HTTP_INTENT_RESPONSE | tr -d '\n' | sed -e 's/.*HTTPINTENTSTATUS://')


echo -e '\033[33;33m' "================(${hotelNm_array[$i]})UTTER==================" '\033[0m'
if [ $HTTP_UTTER_STATUS -eq 200 ]; then
answer=$(echo $HTTP_UTTER_BODY | jq '.answer' | jq '.answer'| sed -e 's/^"//' -e 's/"$//')
echo "ANSWER(5자리) -> " ${answer:0:5}
echo ${hotelNm_array[$i]} "-> 성공 | status_code : " $HTTP_UTTER_STATUS

else echo -e '\033[33;31m' ${hotelNm_array[$i]} "-> 실패 | status_code : " $HTTP_UTTER_STATUS '\033[0m'
fi
echo -e '\033[33;33m' "=======================================" '\033[0m'
echo -e '\033[33;33m' "================(${hotelNm_array[$i]})INTENT==================" '\033[0m'
if [ $HTTP_INTENT_STATUS -eq 200 ]; then
answer=$(echo $HTTP_INTENT_BODY | jq '.answer' | jq '.answer'| sed -e 's/^"//' -e 's/"$//')
echo "ANSWER(5자리) -> " ${answer:0:5}
echo ${hotelNm_array[$i]} "-> 성공 | status_code : " $HTTP_INTENT_STATUS

else echo -e '\033[33;31m' ${hotelNm_array[$i]} "-> 실패 | status_code : " $HTTP_INTENT_STATUS '\033[0m'
fi
echo -e '\033[33;33m' "=======================================" '\033[0m'



done

