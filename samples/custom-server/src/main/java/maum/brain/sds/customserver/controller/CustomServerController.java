package maum.brain.sds.customserver.controller;

import maum.brain.sds.customserver.data.CustomServerApiDto;
import maum.brain.sds.customserver.service.CustomServerCoronaService;
import maum.brain.sds.customserver.service.CustomServerQaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.json.JSONException;

@RestController
@RequestMapping("/custom")
public class CustomServerController {

  @Autowired
  private CustomServerCoronaService coronaService;

  @Autowired
  private CustomServerQaService qaService;

  @RequestMapping(
      value = "/getCoronaCnt",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE
  ) public @ResponseBody
  CustomServerApiDto run(@RequestBody CustomServerApiDto request) throws JSONException {
    return coronaService.getCoronaCnt(request);
  }

  @RequestMapping(
      value = "/findAnswer",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE
  ) public @ResponseBody
  CustomServerApiDto findAnswer(@RequestBody CustomServerApiDto request) throws JSONException {
    return qaService.findAnswer(request);
  }
}
