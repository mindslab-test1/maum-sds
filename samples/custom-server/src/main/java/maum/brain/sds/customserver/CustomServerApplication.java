package maum.brain.sds.customserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomServerApplication.class, args);
	}

}
