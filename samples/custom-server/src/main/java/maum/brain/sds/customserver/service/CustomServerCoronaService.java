package maum.brain.sds.customserver.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import maum.brain.sds.customserver.data.CustomServerApiDto;
import maum.brain.sds.customserver.data.SdsEntity;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import com.google.gson.Gson;

@Service
public class CustomServerCoronaService {
  private Gson gson = new Gson();
  /*
   * 이 샘플 서버에서는 사용자가 알아보기 쉽도록 전처리, 후처리 부분을 분리하였습니다.
   * custom server를 구현할 때, 필요에 따라 구성하시면 됩니다.
   */
  public CustomServerApiDto getCoronaCnt(CustomServerApiDto request) {
    System.out.println("===callCoronaApi request: " + request.toString());

    // DB 또는 특정 api 호출 및 전처리
    String apiResponse = callCoronaApi(request);
    // DB 또는 특정 api의 응답을 후처리 후 return
    return parseCoronaApiResponse(apiResponse);
  }

  /*
   * DB 또는 특정 api를 호출하는 method
   * maum-sds에서 넘어온 파라미터를 DB 또는 특정 api를 호출할 수 있는 형태로 전처리합니다.
   */
  private String callCoronaApi(CustomServerApiDto param) {
    String result;

    try {
      // api 호출 전 비즈니스 로직 처리
//      String serviceKey = "your service key";
      String serviceKey = "dSfpuu841Gew%2F%2FaixE%2B6T3f9ixcldm71bDyIfHzOPgfWHE3Uzb59u1WhZraprjpDPG%2F8%2FJP4MumKVymtQHwBRw%3D%3D";

      Date date = new Date();
      String s = date.toString();
      Calendar c1 = new GregorianCalendar();
      c1.add(Calendar.DATE, -1); // 오늘날짜로부터 -1
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

      String date1 = sdf.format(c1.getTime());
      String date2 = sdf.format(date);

      if (param.getEntities().size() > 0 && param.getEntities().get(0).getEntityValue().trim().equals("어제")) {
        Calendar c2 = new GregorianCalendar();
        c2.add(Calendar.DATE, -2); // 오늘날짜로부터 -2
        date1 = sdf.format(c2.getTime());
        date2 = sdf.format(c1.getTime());
      }

      // 코로나 api 호출
      StringBuilder urlBuilder = new StringBuilder("http://openapi.data.go.kr/openapi/service/rest/Covid19/getCovid19InfStateJson"); /*URL*/
      urlBuilder.append("?" + URLEncoder.encode("ServiceKey","UTF-8") + "=" + serviceKey); /*Service Key*/
      urlBuilder.append("&" + URLEncoder.encode("pageNo","UTF-8") + "=" + URLEncoder.encode("1", "UTF-8")); /*페이지번호*/
      urlBuilder.append("&" + URLEncoder.encode("numOfRows","UTF-8") + "=" + URLEncoder.encode("10", "UTF-8")); /*한 페이지 결과 수*/
      urlBuilder.append("&" + URLEncoder.encode("startCreateDt","UTF-8") + "=" + URLEncoder.encode(date1, "UTF-8")); /*검색할 생성일 범위의 시작*/
      urlBuilder.append("&" + URLEncoder.encode("endCreateDt","UTF-8") + "=" + URLEncoder.encode(date2, "UTF-8")); /*검색할 생성일 범위의 종료*/
      URL url = new URL(urlBuilder.toString());
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestMethod("GET");
      conn.setRequestProperty("Content-type", "application/json");
      System.out.println("Response code: " + conn.getResponseCode());
      BufferedReader rd;
      if (conn.getResponseCode() >= 200 && conn.getResponseCode() <= 300) {
        rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      } else {
        rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
      }
      StringBuilder sb = new StringBuilder();
      String line;
      while ((line = rd.readLine()) != null) {
        sb.append(line);
      }
      rd.close();
      conn.disconnect();
      result = sb.toString();
    } catch (Exception e) {
      System.out.println("rest api server ERR. " + e.getMessage());
      result = "ERROR";
    }

    return result;
  }

  /*
   * 코로나 api response parsing 하는 method
   * response가 xml형식으로 돌아오기 때문에 parsing하여 SdsEntity에 데이터 형식을 맞춰줍니다.
   * response를 받은 후, 비즈니스 로직이 필요하다면 이 부분에서 처리해주면 됩니다.
   */
  private CustomServerApiDto parseCoronaApiResponse(String apiResponse) {
    CustomServerApiDto response = new CustomServerApiDto();

    try {
      if (apiResponse.equals("ERROR")) {
        response.setResultCode("500");
        response.setMessage("get corona api response failed");
        System.out.println("get corona api response failed");
      } else {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(apiResponse)));
        document.getDocumentElement().normalize();

        // XML 데이터 중 <DECIDE_CNT> 태그의 내용을 가져온다.
        NodeList itemList = document.getElementsByTagName("item");
        ArrayList<String> decideCntList = new ArrayList<>();
        // 어제, 오늘의 decideCnt값 추출
        for (int i = 0; i < itemList.getLength(); i++) {
          Element element = (Element) itemList.item(i);
          NodeList nlList = element.getElementsByTagName("decideCnt").item(0).getChildNodes();
          Node nValue = (Node) nlList.item(0);
          decideCntList.add(nValue.getNodeValue());
        }

        int todayDecideCnt =
            Math.abs(
                Integer.parseInt(decideCntList.get(0)) - Integer.parseInt(decideCntList.get(1)));

        SdsEntity sdsEntity = new SdsEntity("Person_Cnt", Integer.toString(todayDecideCnt));
        response.addEntity(sdsEntity);
      }
    } catch (Exception e) {
      response.setResultCode("500");
      response.setMessage(e.getMessage());
      System.out.println("===parseCoronaApiResponse Exception : " + e.getMessage());
    }
    return response;
  }
}
