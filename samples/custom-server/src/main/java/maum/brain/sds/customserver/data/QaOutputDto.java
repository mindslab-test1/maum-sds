package maum.brain.sds.customserver.data;

import java.util.ArrayList;
import java.util.List;

class NewsList {
  String id = "";
  String title = "";
  String sentence = "";
  String context = "";
  int frequency = 0;
  Double answerProb = 0.0;
  Double searchProb = 0.0;
  int sentenceProb = 0;
}

public class QaOutputDto {
    private String answer;
    private Double confidence;
    private int rank;
    private String subject;
    private String property;
    private List<NewsList> newsList;

    public QaOutputDto() {
        answer = "";
        confidence = 0.0;
        rank = 0;
        subject = "";
        property = "";
      newsList = new ArrayList<>();
    }

    public QaOutputDto(String answer, Double confidence, int rank, String subject, String property, List<NewsList> newsLists) {
        this.answer = answer;
        this.confidence = confidence;
        this.rank = rank;
        this.subject = subject;
        this.property = property;
        this.newsList = newsLists;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Double getConfidence() {
        return confidence;
    }

    public void setConfidence(Double confidence) {
        this.confidence = confidence;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getSubject() {
    return subject;
  }

    public void setSubject(String subject) {
    this.subject = subject;
  }

    public String getProperty() {
    return property;
  }

    public void setProperty(String property) {
    this.property = property;
  }

    public List<NewsList> getNewsLists() {
      return newsList;
    }

    public void addNewsList(NewsList newsList) {
      this.newsList.add(newsList);
    }

    public void setNewsList(List<NewsList> newsLists) {
      this.newsList = newsLists;
    }

    public String toString() {
        return "QaOutputDto{" +
                "answer='" + answer + '\'' +
                ", confidence='" + confidence + '\'' +
                ", rank='" + rank + '\'' +
                ", subject='" + subject + '\'' +
                ", property='" + property + '\'' +
                ", newsLists='" + newsList + '\'' +
                '}';
    }
}
