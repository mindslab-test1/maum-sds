package maum.brain.sds.customserver.service;

import com.google.gson.Gson;
import maum.brain.sds.customserver.data.CustomServerApiDto;
import maum.brain.sds.customserver.data.QaDto;
import maum.brain.sds.customserver.data.QaOutputDto;
import maum.brain.sds.customserver.data.SdsEntity;
import org.apache.http.Consts;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

@Service
public class CustomServerQaService {
  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private Gson gson = new Gson();

  private String KBQA_URL = "http://182.162.19.8:51001/kbqa";
  private String TRIPLE_URL = "http://114.108.173.123:5050/ai_human_qa";
  private String WIKI_QA_URL = "http://114.108.173.123:5050/ai_human_qa";
  private String NEWS_QA_URL = "http://114.108.173.123:5050/ai_human_qa";

  public CustomServerApiDto findAnswer(CustomServerApiDto request) {
    logger.info("===findAnswer request: {}", request.toString());
    QaOutputDto tripleResult = callQaApi(request, "triple_wiki", TRIPLE_URL);
    if (tripleResult.getRank() != 0) {
      logger.info("[TRIPLE_WIKI] response: {}", tripleResult.toString());
      return parseQaAnswer(tripleResult, "TRIPLE_WIKI", request);
    } else {
      QaOutputDto kbqaResult = callQaApi(request, "kbqa", KBQA_URL);
      if (kbqaResult.getConfidence() == 1.0) {
        logger.info("[KBQA] response: {}",  kbqaResult.toString());
        return parseQaAnswer(kbqaResult, "KBQA", request);
      } else {
        QaOutputDto wikiResult = callQaApi(request, "wiki", WIKI_QA_URL);
        QaOutputDto newsResult = callQaApi(request, "news", NEWS_QA_URL);
        if (wikiResult.getConfidence() > newsResult.getConfidence()) {
          logger.info("[WIKI] response: {}", wikiResult.toString());
          return parseQaAnswer(wikiResult, "WIKI_MRC", request);
        } else {
          logger.info("[NEWS] response: {}", newsResult.toString());
          return parseQaAnswer(newsResult, "NEWS_MRC", request);
        }
      }
    }
  }

  private QaOutputDto callQaApi(CustomServerApiDto param, String domain, String url) {
    QaOutputDto result = new QaOutputDto();
    CloseableHttpClient client = null;
    CloseableHttpResponse response = null;
    HttpPost post = null;
    JSONObject json = new JSONObject();
    json.put("question", param.getUtter());
    json.put("ntop", 1);
    json.put("domain", domain);

    try {
      client = HttpClientBuilder.create().build();
      post = new HttpPost(url);
      logger.debug("callQaApi request: {}", json.toString());
      post.setEntity(new StringEntity(json.toString(), Consts.UTF_8));
      post.setHeader("Content-type", MediaType.APPLICATION_JSON_VALUE);
      response = client.execute(post);

      if (response.getStatusLine().getStatusCode() == 200) {
        String res = EntityUtils.toString(response.getEntity(), "UTF-8");
        QaDto qaDto = gson.fromJson(res, QaDto.class);
        logger.debug("{} qa result : {}", domain, qaDto.toString());
        if (qaDto.getOutputsSize() > 0) {
          result = qaDto.getOutput(0);
        } else {
          result = new QaOutputDto();
        }
      }
    } catch (Exception e) {
      logger.error("rest api server ERR. {}", e.getMessage());
    }
    return result;
  }

  private CustomServerApiDto parseQaAnswer(QaOutputDto param, String engine, CustomServerApiDto request) {
    CustomServerApiDto result = request;
    try {
      SdsEntity entity = new SdsEntity("QA", param.getAnswer());
      result.setEngine(engine);
      result.setProbability(param.getConfidence());
      result.addEntity(entity);

      if (engine.equals("KBQA")) {
        JSONObject json = new JSONObject();
        json.put("subject", param.getSubject());
        json.put("property", param.getProperty());
        result.setMeta(gson.toJson(json));
      } else {
        result.setMeta(gson.toJson(param.getNewsLists()));
      }
    } catch (Exception e) {
      result.setResultCode("500");
      result.setMessage(e.getMessage());
      logger.error("rest api server ERR. {}", e.getMessage());
    }

    return result;
  }
}
