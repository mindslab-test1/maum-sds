package maum.brain.sds.customserver.data;

import java.util.ArrayList;
import java.util.List;

public class QaDto {
    private List<QaOutputDto> outputs;

    public QaDto() {
        outputs = new ArrayList<>();
    }

    public QaDto(List<QaOutputDto> outputs) {
        this.outputs = outputs;
    }

    public QaOutputDto getOutput(int i) {
        return outputs.get(i);
    }

    public void addOutput(QaOutputDto output) {
        outputs.add(output);
    }

    public void setOutputs(List<QaOutputDto> outputs) {
        this.outputs = outputs;
    }

    public int getOutputsSize() {
        return outputs.size();
    }

    public String toString() {
        return "QaDto{" +
            "outputs='" + outputs + '\'' +
            '}';
    }
}
