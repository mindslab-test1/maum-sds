#-*- coding: utf-8 -*-
from flask import Flask, request
from flask_restful import Api
import os, sys
sys.path.append(os.path.realpath(sys.argv[0] + '/../../'))
from cs_config.config import CUST_CONFIG
from insert_customer import insert_edu_customer
from make_ob_call import ObData
from flask_cors import CORS, cross_origin
import argparse, textwrap

app = Flask(__name__)
api = Api(app)
CORS(app)

# curl -X POST   http://127.0.0.1:6001/update/customList  -H 'Content-Type: application/json'  -H 'cache-control: no-cache'  -d '{
#      "svc_name": "minds-edu"
#      }'
@app.route('/update/customList', methods=['POST'])
def update_custom_list():
    print('/update/customList')
    cust_list = request.json.get('cust_list')
    svc_name = request.json.get('svc_name')
    if svc_name is None or svc_name == '':
        return {'status': 'fail', 'msg': 'no svc-name'}
    if cust_list is None or cust_list == []:
        return {'status': 'fail', 'msg': 'no cust_list'}
    svc_campaign_id = CUST_CONFIG.cust_info.get(svc_name)
    if svc_campaign_id is None or svc_campaign_id == '':
        return {'status': 'fail', 'msg': 'no matching campaign id'}
    insert_edu_customer(svc_campaign_id, cust_list)
    return {'status': 'success', 'campaign_id': svc_campaign_id}

# curl -X POST   http://127.0.0.1:6001/make/ob/call  -H 'Content-Type: application/json'  -H 'cache-control: no-cache'  -d '{
#   "maum_sds_url": "http://182.162.19.19:6961/collect/run/utter",
#   "svc_host": "39",
#   "call_name": "김가나",
#   "campaign": "116",
#   "call_tel": "01000000000",
#   "tester": "mine22@gmail.com"
#   }'
@app.route('/make/ob/call', methods=['POST'])
def make_ob_test_call():
    print('/make/ob/call')
    result = {
        'tester': '',
        'name': '',
        'tel_no': '',
        'contract_no': '',
        'campaign': '',
        'sds_url': '',
        'sds_host': '',
        'lang' : ''
    }
    try:
        maum_sds_url = request.json.get('maum_sds_url')
        svc_host = request.json.get('svc_host')
        call_name = request.json.get('call_name')
        call_tel = request.json.get('call_tel')
        campaign = request.json.get('campaign')
        tester = request.json.get('tester')
        lang = request.json.get('lang')
        result = {'status': 'failed', 'msg': ''}
        if maum_sds_url is None or maum_sds_url == '':
            result['msg'] = 'no maum_sds_url'
            return result
        if svc_host is None or svc_host == '':
            result['msg'] = 'no svc_host'
            return result
        if call_name is None or call_name == '':
            result['msg'] = 'no call_name'
            return result
        if call_tel is None or call_tel == '':
            result['msg'] = 'no call_tel'
            return result
        if tester is None or tester == '':
            result['msg'] = 'no tester'
            return result
        if tester is None or tester == '':
            result['msg'] = 'no campaign'
            return result
        if lang is None or lang == '':
            result['msg'] = 'no lang, 1:kor, 2:eng, 3:jap, 4:chn '
            return result
        ob = ObData()
        result = ob.make_ob_data(campaign, maum_sds_url, svc_host, call_name, call_tel, tester, lang)
    except Exception as e:
        print ('/make/ob/call Failed', str(e))
    finally:
        print (result)
        return result

# curl -X POST   http://127.0.0.1:6001/get/ob/recent  -H 'Content-Type: application/json'  -H 'cache-control: no-cache'  -d '{
#   "campaign": "116",
#   "lang": "1",
#   "tester": "mine22@gmail.com"
#   }'
@app.route('/get/ob/recent', methods=['POST'])
def get_ob_recent():
    print('/get/ob/recent')
    result = {
        'tester': '',
        'name': '',
        'tel_no': '',
        'contract_no': '',
        'campaign': '',
        'sds_url': '',
        'sds_host': ''
    }
    try:
        tester = request.json.get('tester')
        campaign = request.json.get('campaign')
        lang = request.json.get('lang')
        result = {'status': 'failed', 'msg': ''}
        if tester is None or tester == '':
            result['msg'] = 'no tester'
            return result
        if campaign is None or campaign == '':
            result['msg'] = 'no campaign'
            return result
        if lang is None or lang == '':
            result['msg'] = 'no lang, 1:kor, 2:eng, 3:jap, 4:chn '
            return result
        ob = ObData()
        result = ob.get_recent_info(tester, campaign, lang)
    except Exception as e:
        print ('/get/ob/recent Failed', str(e))
    finally:
        print (result)
        return result
# curl -X POST   http://127.0.0.1:6001/get/statistics  -H 'Content-Type: application/json'  -H 'cache-control: no-cache'  -d '{
#   "date": "2020-04-26 11:45:12",
#   "campaign_id": "114"
#   }'
@app.route('/get/statistics', methods = ['POST'])
def get_statistics_func():
    print('/get/statistics')
    result = {'status': 'fail', 'data': []}
    try:
        date = request.json.get('date')
        campaign_id = request.json.get('campaign_id')

        if campaign_id == None or campaign_id == '':
            result['msg'] = 'no campaign id'
            return result
        ob = ObData()
        result = ob.get_statistics(campaign_id, date)
        result['status'] = 'success'
    except Exception as e:
        print('/get/statistics Failed', str(e))
    finally:
        return result

# curl -X POST   http://127.0.0.1:6001/get/test_ob  -H 'Content-Type: application/json'  -H 'cache-control: no-cache'  -d '{
#   "tel_no": "01020202020"
#   }'
@app.route('/get/test_ob', methods = ['POST'])
def get_test_ob():
    print('/get/test_ob')
    result = ''
    try:
        tel_no = request.json.get('tel_no')
        if tel_no == None or tel_no == '':
            return {'status': 'fail', 'msg': 'no campaign id'}

        signInParam = {
            'userKey': 'admin',
            'passPhrase': '1234'
        }
        headers = {'m2u-auth-internal': 'm2u-auth-internal',
            'Content-Type': 'application/json',
            'cache-control': 'no-cache'
        }
        # const HEADERS = {headers: {'m2u-auth-internal': 'm2u-auth-internal'}};
        url = 'http://10.122.64.152:9990/api/v3/auth/signIn'
        # data_param = {'contractno': str(contractno), 'intent': str(intent)}
        print('##gogo > ', url, signInParam)
        response = requests.post(url, headers=headers, data=json.dumps(signInParam))
        print('##res')
        print(str(response))
        if response is not None and response.ok:
            print(response)
            print(response.content)
            if 'directive' in response:
                directive = response['directive']
                print(directive)
                authKey = ''
                authKey = directive['payload']['authSuccess']
                openParam = {
                    'payload': {
                        'utter': 'UTTER1',
                        'lang': 0,
                        'chatbot': 'AI_SIMPLE_VC_BOT',
                        'meta': {
                            'debug': 'true'
                        }
                    },
                    'device': {
                        'id': 'AwsConnect',
                        'type': 'WEB',
                        'version': '0.1',
                        'channel': 'ADMINWEB'
                    },
                    'location': {
                        'latitude': 10.3,
                        'longitude': 20.5,
                        'location': 'mindslab'
                    },
                    'authToken': authKey
                }
                openParam['device']['id'] = 'AwsConnect'
        return {'status': 'success', 'data': result}
    except Exception as e:
        print('/get/test_ob', str(e))
        return {'status': 'failed', 'message': 'get/test_ob failed'}

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Custom BL Server",
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("-p", "--port",
                        nargs="?",help=textwrap.dedent('port number'))
    args = parser.parse_args()
    cust_bl_port = 6001
    if args and args.port:
        cust_bl_port = args.port
    app.run(host='10.122.64.152', port=cust_bl_port, debug=True)
