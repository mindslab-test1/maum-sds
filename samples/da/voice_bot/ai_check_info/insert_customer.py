# -*- coding:utf-8 -*-
# GET 요청 기준
import requests

from config.config import HCallMysqlConfig
from ai_da_lib.dbconnector import DbConnector
from ai_sql import SQLSET

def insert_edu_customer(campain_id, data):
    sqls = SQLSET

    edu_mysql = DbConnector(HCallMysqlConfig)

    cust_info = {}
    count = 1
    # recall hitory 데이터도 contract_id로 join 걸어서 데이터 삭제
    edu_mysql.execute_query_with_params(sqls.DELETE_RECALL_HISTORY_JOIN_CM_CONTRACT, (campain_id))
    # api 호출시 기존 cm_contract 등록된 발송 예정 고객 데이터 삭제
    edu_mysql.execute_query_with_params(sqls.DELETE_CM_CONTRACT, (campain_id))

    for member in data:
        print(count)
        print(member)
        if member['mem_hp']:
            cust_info['CUST_TEL_NO'] = str(member['mem_hp']).replace('-', '') # mem_hp가 있으면 cust_info['CUST_TEL_NO'] = mem_hp
        if member['mem_name']:
            cust_info['CUST_NM'] = member['mem_name']  # mem_name가 있으면 cust_info['CUST_TEL_NO'] = mem_name
        if member['mem_id']:
            cust_info['CUST_ID'] = member['mem_id']  # mem_od가 있으면 cust_info['CUST_TEL_NO'] = mem_id
        print('CUST_TEL_NO : {}, CUST_NM : {}, CUST_ID : {}_', cust_info['CUST_TEL_NO'], cust_info['CUST_NM'], cust_info['CUST_ID'])

        row = edu_mysql.execute_with_params(sqls.SELECT_CHECK_DUPLIY_CUST_INFO,
                                          (cust_info['CUST_NM'], cust_info['CUST_TEL_NO'], cust_info['CUST_ID']))

        row2 = edu_mysql.execute_with_params(sqls.SELECT_CHECK_DUPLIY_CM_CONTRACT,
                                           (cust_info['CUST_TEL_NO'], "114"))

        print(sqls.SELECT_CHECK_DUPLIY_CUST_INFO,
                                          (cust_info['CUST_NM'], cust_info['CUST_TEL_NO']))
        print('row : ', row)
        print(sqls.SELECT_CHECK_DUPLIY_CM_CONTRACT,
                                          (cust_info['CUST_TEL_NO'], campain_id))
        print('row2 : ', row2)
        if row is None: # CUST_BASE_INFO 조회해서 중복된게 없는것만 insert 로직
            if row2 is None : # CM_CONTRACT 테이블 조회해서 전화번호와 캠페인번호가 중첩된게 없다면
                print(sqls.INSERT_CUST_INFO, (cust_info['CUST_NM'], '', cust_info['CUST_TEL_NO'],
                                                                    '', '', '', '', '', '', '', '',
                                                                    '', 0, 0, '', '', '', '', '',
                                                                    '', ''))
                edu_mysql.insertexecute_with_params(sqls.INSERT_CUST_INFO, (cust_info['CUST_NM'], '',
                                                                          cust_info['CUST_TEL_NO'], '', '', '', '', '',
                                                                          '', '', '', '', 0, 0, '', cust_info['CUST_ID'], '', '', '', '',
                                                                          ''))
                row3 = edu_mysql.execute_with_params('select CUST_ID from CUST_BASE_INFO where CUST_NM=%s and CUST_TEL_NO = %s',
                                          (cust_info['CUST_NM'], cust_info['CUST_TEL_NO']))
                if row3 is not None:
                    print(sqls.INSERT_CM_CONTRACT, (campain_id, row3[0], '', '', '', '', '', '', '', '', ''))
                    edu_mysql.insertexecute_with_params(sqls.INSERT_CM_CONTRACT, (campain_id, row3[0], cust_info['CUST_TEL_NO'], 'voice3', 0, 0, 0, 'N', 0,
                                                                            0, 0))
            else:
                print("이미 등록된 고객입니다.[CM_CONTRACT] : ", cust_info['CUST_NM'], cust_info['CUST_TEL_NO'])
        else:
            print("이미 등록된 고객입니다.[CUST_BASE_INFO]", cust_info['CUST_NM'], cust_info['CUST_TEL_NO'])
            if row2 is None : # CM_CONTRACT 테이블 조회해서 전화번호와 캠페인번호가 중첩된게 없다면
                row3 = edu_mysql.execute_with_params('select CUST_ID from CUST_BASE_INFO where CUST_NM=%s and CUST_TEL_NO = %s',
                                          (cust_info['CUST_NM'], cust_info['CUST_TEL_NO']))
                if row3 is not None:
                    print(sqls.INSERT_CM_CONTRACT, (campain_id, row3[0], '', '', '', '', '', '', '', '', ''))
                    edu_mysql.insertexecute_with_params(sqls.INSERT_CM_CONTRACT, (campain_id, row3[0], cust_info['CUST_TEL_NO'], 'voice3', 0, 0, 0, 'N', 0,
                                                                            0, 0))
            else:
                print("이미 등록된 고객입니다.[CM_CONTRACT] : ", cust_info['CUST_NM'], cust_info['CUST_TEL_NO'])
        count += 1
        # break

if __name__ == "__main__":
    insert_edu_customer('114') # 전화영어 campain에 고객데이터 insert
