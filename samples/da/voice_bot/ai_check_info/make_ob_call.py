# -*- coding:utf-8 -*-
# GET 요청 기준
import sys, os
sys.path.append(os.path.realpath(sys.argv[0] + '/../../'))
from config.config import HCallMysqlConfig, MysqlVoiceConfig
from ai_da_lib.dbconnector import DbConnector
from ai_sql import SQLSET
from datetime import datetime
import math

sqls = SQLSET()
hc_mysql = DbConnector(HCallMysqlConfig)
ai_mysql = DbConnector(MysqlVoiceConfig)

class ObData:
    def __init__(self):
        print('init')

    def make_ob_data(self, campaign, maum_sds_url, svc_host, call_name, call_tel, tester, lang):
        status = 'failed'
        result = {
            'tester': tester,
            'name': call_name,
            'tel_no': call_tel,
            'contract_no': '',
            'campaign': campaign,
            'sds_url': maum_sds_url,
            'sds_host': svc_host,
            'lang': lang
        }
        try:
            cust_id = None
            contract_no = None

            par1 = (call_name, call_tel)
            res1 = hc_mysql.execute_with_param(sqls.CHECK_TEL, call_tel)
            if res1 is None:
                res2 = hc_mysql.execute_query_with_params(sqls.INSERT_TEL, par1)
                if res2 is None and res2 < 1:
                    print ('[INSERT CUSTOM INFO] Failed to insert customer information')
                    return {'status': status, 'result': result}
                else:
                    cust_id = res2
                    print ('[INSERT CUSTOM INFO] Insert customer information successfully. / ', cust_id)
            else:
                cust_id = res1[0]
                print ('[GET CUSTOM INFO] Customer information already existed. / ', cust_id)

            if cust_id is not None:
                #res3 = hc_mysql.execute_with_params(sqls.CHECK_CONTRACT, par2)
                ## 일단 영어버전 관리자계정 설정위해 하드 코딩해놓겠음
                admin = 'fast1'
                if campaign == '121':
                    admin = 'simple1'
                par2 = (campaign, cust_id, call_tel, admin)
                res4 = hc_mysql.execute_query_with_params(sqls.INSERT_CONTRACT, par2)
                #if res3 is None:
                #    res4 = hc_mysql.execute_query_with_params(sqls.INSERT_CONTRACT, par2)
                if res4 is None or res4 < 1:
                    print ('[INSERT CONTRACT] Failed to insert contract information.')
                    return {'status': status, 'result': result}
                else:
                    contract_no = str(res4)
                    print ('[INSERT CONTRACT] Insert contract information successfully.')
                #else:
                #    contract_no = str(res3[0])
                #    print ('[GET CONTRACT INFO] Contract information already existed./ ', contract_no)
                result['contract_no'] = contract_no
            else:
                return {'status': status, 'result': result}
            if contract_no is not None:
                par5 = (tester, campaign, lang)
                res5 = ai_mysql.execute_with_params(sqls.CHECK_OB_INFO, par5)
                par6 = (tester, maum_sds_url, svc_host, campaign, lang)
                if res5 is None:
                    res6 = ai_mysql.execute_query_with_params(sqls.INSERT_OB_USR_INFO, par6)
                    if res6 is not None:
                        print ('[INSERT OB USR DATA] Insert outbound user information successfully.')
                        par7 = (contract_no, cust_id, call_name, campaign, call_tel, res6)
                        res7 = ai_mysql.execute_query_with_params(sqls.INSERT_OB_INFO, par7)
                        if res7 is not None:
                            print ('[INSERT OB DATA] Insert outbound customer information successfully.')
                            status = 'success'
                        else:
                            print ('[INSERT OB DATA] Failed to insert outbound customer information.')
                    else:
                        print ('[INSERT OB USR DATA] Failed to insert outbound user information.')
                else:
                    if None in (res5[0], res5[1], res5[2], res5[3], res5[4], res5[6]):
                        print ('[GET OB USR DATA] outbound user information already existed.')
                        par7 = (contract_no, cust_id, call_name, campaign, call_tel, str(res5[7]))
                        res7 = ai_mysql.execute_query_with_params(sqls.INSERT_OB_INFO, par7)
                        if res7 is not None and res7 > -1:
                            print ('[INSERT OB USR DATA] Insert outbound customer information successfully.')
                            status = 'success'
                        else:
                            print ('[INSERT OB USR DATA] Failed to insert outbound customer information.')
                    else:
                        print ('[GET OB USR DATA] outbound user information already existed.')
                        print ('[GET OB CUST DATA] outbound customer information already existed.')
                        tester_id = str(res5[6])
                        par8 = (maum_sds_url, svc_host, campaign, tester_id)
                        par9 = (contract_no, cust_id, call_name, campaign, call_tel, tester_id)
                        res8 = ai_mysql.update_query_with_params(sqls.UPDATE_OB_USR_INFO, par8)
                        if res8 is not None and res8 > -1:
                            print ('[UPDATE OB USR DATA] Update outbound user information successfully.')
                            res9 = ai_mysql.update_query_with_params(sqls.UPDATE_OB_INFO, par9)
                            if res9 is not None  and res9 > -1:
                                status = 'success'
                                print ('[UPDATE OB CUST DATA] Update outbound customer information successfully.')
                            else:
                                print ('[UPDATE OB CUST DATA] Failed to update outbound customer information.')
                        else:
                            print ('[UPDATE OB USR DATA] Failed to update outbound user information.')
            else:
                return {'status': status, 'result': result}

            return True
        except Exception as e:
            print ('[Make OB Data] END Failed', str(e))
        finally:
            print (status, result)
            return {'status': status, 'result': result}


    def get_recent_info(self, tester, campaign, lang):
        status = 'failed'
        result = {
            'tester': tester,
            'name': '',
            'tel_no': '',
            'contract_no': '',
            'campaign': campaign,
            'sds_url': '',
            'lang': lang,
            'sds_host': ''
        }
        try:
            # GET OB DATA
            par = (tester, campaign, lang)
            print(sqls.CHECK_OB_INFO, par)
            res = ai_mysql.execute_with_params(sqls.CHECK_OB_INFO, par)
            if res is  not None:
                print (res)
                print ('[GET OB DATA] Get outbound information successfully.')
                result['name'] = res[0]
                result['contract_no'] = res[1]
                result['campaign'] = res[2]
                result['tel_no'] = res[3]
                result['sds_url'] = res[4]
                result['sds_host'] = res[5]
                status = 'success'
            else:
                print ('[GET OB DATA] Failed to get outbound information.')
        except Exception as e:
            print ('[Get recent OB Data] Failed', str(e))
        finally:
            print (status, result)
            return {'status': status, 'result': result}

    def get_statistics(self, campaign_id, date):
        result = []
        rows = None
        tasks = None
        try:
            if date == None or date == '':
                now = datetime.now()
                date = now.strftime("%Y-%m-%d 00:00:00")
                print("set date and time =", date)
            print(sqls.GET_TASKS, campaign_id)
            rows = hc_mysql.execute_all_with_param(sqls.GET_TASKS, campaign_id)
            print(rows)
            if rows is not None:
                tasks = [str(item[0]) for item in rows]
            if tasks is not None or tasks is not []:
                cnt = 10 - len(tasks)
                if cnt > 0:
                    tmp_app = [''] * cnt
                    tasks = tasks + tmp_app
                print(cnt)
                pars = tasks
                pars.append(campaign_id)
                pars.append(campaign_id)
                pars.append(date)
                pars = tuple(pars)
                print(sqls.GET_STATISTICS, pars)
                rows = hc_mysql.execute_all_with_params(sqls.GET_STATISTICS, pars)
                if rows is not None:
                    # /SELECT D.CUST_NM, A.CALL_ID, A.STS_VAL, DATE_FORMAT(A.CREATED_DTM, '%%Y-%%m-%%d %%T') AS 'CV_DT'
                    for item in rows:
                        name = item[0] #name
                        sts_val = math.trunc(item[2]) #sts_val
                        cv_dt = item[3] #cv_dt
                        call_id = str(item[1]) #call_id
                        tmp_list = list("%010d"% sts_val)
                        tmp_st_list = list(map(lambda x : '1' if int(x) > 1 else x, tmp_list))
                        sts_val_str = ','.join(tmp_st_list)
                        # print(sts_val_str)
                        val = str(name + ',' +
                                  sts_val_str + ',' +
                                  cv_dt + ',' +
                                  call_id)
                        result.append(val)
                        # print(result)
        except Exception as e:
            print ('[Get recent OB Data] Failed', str(e))
        finally:
            # print('====data====')
            # print(rows)
            # print('=====result===')
            # print(result)
            return result



if __name__ == "__main__":
    maum_sds_url = "http://182.162.19.19:6961/collect/run/utter"
    svc_host = "39"
    campaign = "116"
    call_name = "김꽃님"
    call_tel = '01000001113'
    tester = "guest2@gmail.com"
    # result = make_ob_data(campaign, maum_sds_url, svc_host, call_name, call_tel, tester)
    ob = ObData()
    result = ob.get_recent_info(tester)
    print (result)
