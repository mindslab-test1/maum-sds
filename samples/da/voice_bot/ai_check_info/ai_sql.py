#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys

class SQLSET:
    CONTRACT_TABLE = 'CM_CONTRACT'
    CUST_BASE_INFO = 'CUST_BASE_INFO'
    CM_CONTRACT = 'CM_CONTRACT'

    BIRTH_BY_CONTRACTNO = 'select left(JUMIN_NO, 6) as BIRTH from ' + CUST_BASE_INFO + ' where CUST_ID = %s '

    CUSTINFO_BY_CONTRACTNO = 'select left(b.JUMIN_NO, 6) as BIRTH, TEL_NO, CUST_NM, a.CAMPAIGN_ID, a.CUST_ID  from ' + CONTRACT_TABLE + \
                             ' a left join ' + CUST_BASE_INFO + \
                             ' b on a.CUST_ID = b.CUST_ID where a.CONTRACT_NO=%s'

    CUSTNM_BY_CONTRACTNO = 'select CUST_NM, a.CAMPAIGN_ID, a.CUST_ID  from ' + CONTRACT_TABLE + \
                             ' a left join ' + CUST_BASE_INFO + \
                             ' b on a.CUST_ID = b.CUST_ID where a.CONTRACT_NO=%s'

    CUSTNO_BY_CONTRACTNO = 'select  a.CAMPAIGN_ID, a.CUST_ID, b.CUST_NM  from ' + CONTRACT_TABLE + \
                             ' a left join ' + CUST_BASE_INFO + \
                             ' b on a.CUST_ID = b.CUST_ID where a.CONTRACT_NO=%s'

    HISTORY_INFO_BY_CONTRACTNO = 'select a.CAMPAIGN_ID, a.CUST_ID, b.CUST_NM, a.CALL_TRY_COUNT, left(b.JUMIN_NO, 7) as BIRTH, b.CUST_TEL_NO ' \
                                 'from ' + CONTRACT_TABLE + \
                           ' a left join ' + CUST_BASE_INFO + \
                           ' b on a.CUST_ID = b.CUST_ID where a.CONTRACT_NO=%s'

    CUST_API_ID_BY_CUST_NO = 'select CUST_API_ID from ' + CUST_BASE_INFO + ' where CUST_ID=%s'

    CUSTLIST_BY_CAMPID = 'select prod_name, month_fee, tot_fee, unpaid_year, unpaid_month, tot_month from cust_list where campaign_id=%s and cust_id=%s '
    SCENARIO_BY_INTENT = 'select call_path, intent_order, ai_ect, socket_task_seq, detect_type from ai_scenario where campaign_id=%s and intent=%s'
    SCENARIO_BY_ORDER = 'select call_path, intent_order from ai_scenario where campaign_id=%s and intent="처음으로"'
    UPDATE_DETECT = "insert into CM_CAMPAIGN_SCORE (CALL_ID,CONTRACT_NO,CAMPAIGN_ID,INFO_SEQ,INFO_TASK,TASK_VALUE,CREATOR_ID,UPDATER_ID,CREATED_DTM,UPDATED_DTM) values (%s,%s,%s,%s,%s,%s,'100','100',sysdate(),sysdate())"
    TASK_INFO = 'select TASK,SEQ from CM_CAMPAIGN_INFO where CAMPAIGN_ID=%s and CAMPAIGN_INFO_ID=%s'
    MAKE_SCENARIO_INFO = 'INSERT INTO ai_scenario (campaign_id, intent_order, intent, call_path, createDt, descript, creator, ai_ect, socket_task_seq, detect_type) VALUES (%s, %s, %s, %s, SYSDATE(), null, %s, %s, %s, %s)'
    MAKE_TASK_INFO ='INSERT INTO CM_CAMPAIGN_INFO (SEQ, CAMPAIGN_ID, CATEGORY, TASK, TASK_TYPE, TASK_ANSWER, TASK_INFO, CREATOR_ID, UPDATER_ID, CREATED_DTM, UPDATED_DTM) VALUES (%s, %s, %s, %s, %s, %s, %s, null, null, SYSDATE(), SYSDATE())'
    CHECK_TASK_INFO ='select CAMPAIGN_INFO_ID From CM_CAMPAIGN_INFO where CAMPAIGN_ID=%s and TASK_INFO=%s'
    DEL_SCENARIO = 'delete from ai_scenario where campaign_id=%s'
    CUSTLIST_BY_CUSTNO = 'select  seq_no,prod_name,month_fee,unpaid_year,unpaid_month,cust_id,cust_name,campaign_id,tot_fee,tot_month, user_id,tel_no  from cust_list where cust_id=%s'

    CUSTLIST_BY_CAMPAIGNNO_AND_CONTRACTNO = 'select  seq_no,prod_name,month_fee,unpaid_year,unpaid_month,cust_id,cust_name,campaign_id,tot_fee,tot_month, user_id,tel_no  from cust_list where campaign_id=%s and contract_no=%s'

    INSERT_CUST_INFO = """insert into CUST_BASE_INFO
                        (CUST_NM,JUMIN_NO,CUST_TEL_NO,CUST_COMPANY_NO,CUST_HOME_NO,CUST_ETC_NO,CUST_TEL_COMP,TEL_COMP_SAVE_YN,CERTIFI_NO,CREATE_DT,MODIFY_DT,CUST_ADDRESS,
    		     CUST_DETAIL_ADDRESS,CONSULT_CHAT_ID,CUST_TYPE,CUST_STATE,CUST_SUBSC_PLAN,CUST_API_ID,CUST_API_KEY,CUST_REG_DATE,CUST_REG_PATH,CUST_TERM_DATE,CUST_ADDRESS2,
                         CUST_DETAIL_ADDRESS2,CUST_EMAIL)
                        values 
                        (%s,%s,%s,%s,%s,%s,%s,%s,%s,sysdate(),sysdate(),%s,%s,%s,%s,%s,%s,%s,%s,sysdate(),%s,sysdate(),%s,%s,%s)"""

    INSERT_CM_CONTRACT = """insert into CM_CONTRACT
                        (CAMPAIGN_ID,CUST_ID,TEL_NO,CUST_OP_ID,PROD_ID,CALL_TRY_COUNT,LAST_CALL_ID,IS_INBOUND,TASK_SEQ,CREATOR_ID,UPDATER_ID,CREATED_DTM,UPDATED_DTM)
                        values 
                        (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,sysdate(),sysdate())"""

    SELECT_CHECK_DUPLIY_CUST_INFO = 'select * from CUST_BASE_INFO where CUST_NM=%s and CUST_TEL_NO=%s and CUST_API_ID=%s group by CUST_TEL_NO'
    SELECT_CHECK_DUPLIY_CM_CONTRACT = 'select * from CM_CONTRACT where TEL_NO = %s and CAMPAIGN_ID = %s'

    SAVE_HISTORY = 'INSERT INTO voice_bot.ai_history (campaign_id, cust_id, cust_name, call_status, fail_msg, last_intent,' \
                   ' lm_transfer, vir_transfer, ext_number, tel_no, gender, ages, etc, call_cnt, etc2, etc3, contract_no, call_st_dt, ' \
                   'call_ed_dt, call_ing_dt, success) VALUES (%s, %s, %s, %s, %s,' \
                   ' %s, %s, %s, %s, %s,' \
                   ' %s, %s, %s, %s, %s,' \
                   ' %s, %s, %s,sysdate(), TIMEDIFF(sysdate(), %s), %s)'

    SAVE_SIMPLE_HISTORY = 'INSERT INTO voice_bot.ai_history (campaign_id, cust_id, cust_name, call_status, fail_msg,' \
                          'last_intent, ext_number, tel_no, gender, ages,' \
                          'etc, call_cnt, contract_no, call_st_dt, call_ed_dt,' \
                          'call_ing_dt, success) VALUES (' \
                          ' %s, %s, %s, %s, %s,' \
                          ' %s, %s, %s, %s, %s,' \
                          ' %s, %s, %s, %s, sysdate(),' \
                          ' TIMEDIFF(sysdate(), %s), %s)'

    # simple intent
    SCENARIO_BY_INTENT_STEP = "select call_path, intent_order, ai_ect, socket_task_seq, detect_type from ai_scenario where campaign_id=%s and intent like %s"
    SCENARIO_BY_ORDER_STEP = "select call_path, intent_order from ai_scenario where campaign_id=%s and intent='처음으로'"

    # delete cm_contract
    DELETE_CM_CONTRACT = 'delete from CM_CONTRACT where CAMPAIGN_ID=%s'

    # delete recall_history
    DELETE_RECALL_HISTORY_JOIN_CM_CONTRACT = 'delete B from CM_CONTRACT as A join RECALL_HISTORY as B ' \
                                             'where B.CONTRACT_NO = A.CONTRACT_NO and A.CAMPAIGN_ID=%s'

    CHECK_SUCCESS = "select intent from ai_scenario  where campaign_id=%s and ai_success=1 and intent=%s"


    CHECK_TEL = "select CUST_ID from CUST_BASE_INFO a where CUST_TEL_NO=%s"
    INSERT_TEL = "INSERT INTO CUST_BASE_INFO ( CUST_NM, CUST_TEL_NO, CREATE_DT, MODIFY_DT) VALUES (%s, %s, sysdate(), sysdate())"
    CHECK_CONTRACT = "select * from CM_CONTRACT where CAMPAIGN_ID = %s and CUST_ID=%s and TEL_NO=%s"
    INSERT_CONTRACT = "INSERT INTO CM_CONTRACT (CAMPAIGN_ID, CUST_ID, TEL_NO, CUST_OP_ID, IS_INBOUND, CREATED_DTM, UPDATED_DTM) VALUES (%s, %s, %s, %s, 'Y', sysdate(), sysdate())"
    # INSERT_CONTRACT = "INSERT INTO CM_CONTRACT (CAMPAIGN_ID, CUST_ID, TEL_NO, CUST_OP_ID, IS_INBOUND, CREATED_DTM, UPDATED_DTM) VALUES (%s, %s, %s, 'fast1', 'Y', sysdate(), sysdate())"

    CHECK_OB_INFO = "select b.cust_name, b.contract_no, a.campaign_id, b.tel_no, a.maum_sds_url, a.svc_host, b.tester_id, a.id from simplebot_user_info a left join cust_list b on (a.id = b.tester_id) where a.tester = %s and a.campaign_id=%s and a.lang = %s"
    # CHECK_OB_INFO = "select b.cust_name, b.contract_no, a.campaign_id, b.tel_no, a.maum_sds_url, a.svc_host, b.tester_id, a.id from simplebot_user_info a left join cust_list b on (a.id = b.tester_id) where a.tester = %s"
    INSERT_OB_INFO = "INSERT INTO cust_list (contract_no, cust_id, cust_name, campaign_id, tel_no, tester_id, prod_name, month_fee, unpaid_year, unpaid_month, tot_fee, tot_month, user_id) VALUES (%s, %s, %s, %s, %s, %s, '', '', '', '', '', '', '')"
    INSERT_OB_USR_INFO = "INSERT INTO simplebot_user_info (tester, maum_sds_url, svc_host, campaign_id, lang) VALUES (%s, %s, %s, %s, %s)"
    UPDATE_OB_USR_INFO = "update simplebot_user_info set maum_sds_url=%s, svc_host=%s,campaign_id=%s where id=%s"
    UPDATE_OB_INFO = "update cust_list set contract_no=%s, cust_id=%s, cust_name=%s, campaign_id=%s, tel_no=%s where tester_id=%s"

    GET_SDS_INFO = "select a.maum_sds_url, a.svc_host, a.campaign_id, b.cust_name, a.lang from simplebot_user_info a left join cust_list b on (a.id = b.tester_id) where b.contract_no = %s"

    GET_CALL_QUEUE = "select a.rownum, a.CONTRACT_NO from (select CONTRACT_NO, CAMPAIGN_ID, @rownum:=@rownum+1 as rownum from OB_CALL_QUEUE, (select @rownum:=0) r where CAMPAIGN_ID=%s) a where a.CONTRACT_NO = %s"

    GET_TASKS = "select CAMPAIGN_INFO_ID from CM_CAMPAIGN_INFO where  CAMPAIGN_ID = %s order by CAMPAIGN_INFO_ID"
    GET_STATISTICS = """

SELECT D.CUST_NM, A.CALL_ID, A.STS_VAL, DATE_FORMAT(A.CREATED_DTM, '%%Y-%%m-%%d %%T') AS 'CV_DT', A.DEBUG1, A.DEBUG2, A.DEBUG3 FROM (
                             select CALL_ID,
                                    CONTRACT_NO,
                                    CREATED_DTM,
                                    SUM(BIN(VAL)) AS STS_VAL,
                                    GROUP_CONCAT(INFO_SEQ) AS DEBUG1,
                                    GROUP_CONCAT(TASK_VALUE) AS DEBUG2,
                                    GROUP_CONCAT(BIN(VAL), ' ') AS DEBUG3
                             FROM (
                                    select CALL_ID,
                                           INFO_SEQ,
                                           TASK_VALUE,
                                           CREATED_DTM,
                                           CONTRACT_NO,
                                           (
                                             CASE
                                               WHEN INFO_SEQ = %s and TASK_VALUE = 'Y' THEN POW(2, 9)
                                               WHEN INFO_SEQ = %s and TASK_VALUE = 'Y' THEN POW(2, 8)
                                               WHEN INFO_SEQ = %s and TASK_VALUE = 'Y' THEN POW(2, 7)
                                               WHEN INFO_SEQ = %s and TASK_VALUE = 'Y' THEN POW(2, 6)
                                               WHEN INFO_SEQ = %s and TASK_VALUE = 'Y' THEN POW(2, 5)
                                               WHEN INFO_SEQ = %s and TASK_VALUE = 'Y' THEN POW(2, 4)
                                               WHEN INFO_SEQ = %s and TASK_VALUE = 'Y' THEN POW(2, 3)
                                               WHEN INFO_SEQ = %s and TASK_VALUE = 'Y' THEN POW(2, 2)
                                               WHEN INFO_SEQ = %s and TASK_VALUE = 'Y' THEN POW(2, 1)
                                               WHEN INFO_SEQ = %s and TASK_VALUE = 'Y' THEN POW(2, 0)
                                               ELSE 0 END
                                             ) AS VAL
                                    from (
                                           select CALL_ID, INFO_SEQ, TASK_VALUE, CREATED_DTM, CONTRACT_NO
                                           from (
                                                  select a.CALL_ID,
                                                         a.INFO_SEQ,
                                                         a.TASK_VALUE,
                                                         a.CREATED_DTM,
                                                         a.CONTRACT_NO,
                                                         (case @vTask
                                                            when a.CALL_ID
                                                              then @rownum := @rownum + 1
                                                            ELSE @rownum := 1 END) rnum,
                                                         (@vTask := a.CALL_ID)     vTask
                                                  from CM_CAMPAIGN_SCORE a,
                                                       (SELECT @vTask := '', @rownum := 0 FROM DUAL) b
                                                  where a.CAMPAIGN_ID = %s
                                                ) B
                                         ) P
                                  ) C
                             group by C.CALL_ID

                           ) A LEFT JOIN ( SELECT C.CUST_NM, B.CONTRACT_NO FROM CM_CONTRACT B
                                                                                  LEFT JOIN CUST_BASE_INFO C ON B.CUST_ID = C.CUST_ID WHERE B.CAMPAIGN_ID = %s) D ON A.CONTRACT_NO = D.CONTRACT_NO
where A.CREATED_DTM > %s"""

# SELECT D.CUST_NM, A.TASK1, A.TASK2, A.TASK3, A.TASK4, A.TASK5, A.TASK6, A.TASK7, A.TASK8, A.TASK9, A.TASK10, A.CV_DT  FROM (SELECT  P.CALL_ID, P.CONTRACT_NO AS CONTRACT_NO,
#      (CASE WHEN P.INFO_SEQ = %s and P.TASK_VALUE = 'Y' THEN 1 ELSE 0 END) AS 'TASK1',
#      (CASE WHEN P.INFO_SEQ = %s and P.TASK_VALUE = 'Y' THEN 1 ELSE 0 END) AS 'TASK2',
#      (CASE WHEN P.INFO_SEQ = %s and P.TASK_VALUE = 'Y' THEN 1 ELSE 0 END) AS 'TASK3',
#      (CASE WHEN P.INFO_SEQ = %s and P.TASK_VALUE = 'Y' THEN 1 ELSE 0 END) AS 'TASK4',
#      (CASE WHEN P.INFO_SEQ = %s and P.TASK_VALUE = 'Y' THEN 1 ELSE 0 END) AS 'TASK5',
#      (CASE WHEN P.INFO_SEQ = %s and P.TASK_VALUE = 'Y' THEN 1 ELSE 0 END) AS 'TASK6',
#      (CASE WHEN P.INFO_SEQ = %s and P.TASK_VALUE = 'Y' THEN 1 ELSE 0 END) AS 'TASK7',
#      (CASE WHEN P.INFO_SEQ = %s and P.TASK_VALUE = 'Y' THEN 1 ELSE 0 END) AS 'TASK8',
#      (CASE WHEN P.INFO_SEQ = %s and P.TASK_VALUE = 'Y' THEN 1 ELSE 0 END) AS 'TASK9',
#      (CASE WHEN P.INFO_SEQ = %s and P.TASK_VALUE = 'Y' THEN 1 ELSE 0 END) AS 'TASK10', P.CREATED_DTM, DATE_FORMAT(P.CREATED_DTM, '%%Y-%%m-%%d %%T') AS 'CV_DT'
#      FROM CM_CAMPAIGN_SCORE P
#      where CAMPAIGN_ID = %s
#      GROUP BY P.CALL_ID) A LEFT JOIN ( SELECT C.CUST_NM, B.CONTRACT_NO FROM CM_CONTRACT B
#          LEFT JOIN CUST_BASE_INFO C ON B.CUST_ID = C.CUST_ID WHERE B.CAMPAIGN_ID = %s) D ON A.CONTRACT_NO = D.CONTRACT_NO
# where A.CREATED_DTM > %s
