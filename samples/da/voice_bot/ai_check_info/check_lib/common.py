#!/usr/bin/python
# -*- coding: utf-8 -*-
import re

class Common:
    def number_to_kor(self, text):
        print('CONVERT NUM TO KOR start')
        res = text
        nums = {'0': '공', '1': '일', '2': '이', '3': '삼', '4': '사', '5': '오', '6': '육', '7': '칠', '8': '팔',
                '9': '구'}
        target_list = nums.keys()
        # print(target_list)
        for one in target_list:
            res = res.replace(one, nums[one])
        print('NUM : {} \n ==> KOR: {}'.format(text, res))
        print('CONVERT NUM LIST TO KOR end')
        return res

    def kor_to_number(self, text):
        print('CONVERT KOR TO NUM start')
        res = text.replace(' ','')
        nums = {'공': '0', '영': '0',  '일': '1', '이': '2', '삼': '3', '사': '4', '오': '5', '육': '6', '칠': '7', '팔': '8',
                '구': '9','하나': '1', '둘': '2', '셋': '3', '넷': '4', '다섯': '5', '여섯': '6', '일곱': '7', '여덟': '8',
                '아홉': '9'}
        # target_list = nums.keys()
        for one in target_list:
            res = res.replace(one, nums[one])
        print('KOR : {} \n ==> NUM: {}'.format(nums, res))
        print('CONVERT KOR TO NUM end')
        return res

    def eng_to_kor(self, text):
        print('CONVERT NUM TO KOR start')
        res = text.upper()
        eng = {'A': '에이', 'B': '비','C': '씨','D': '디','E': '이','F': '에프','G': '쥐','H': '에이치', 'I': '아이','J': '제이',
                'K': '케이', 'L': '엘','M': '엠','N': '앤','O': '오','P': '피','Q': '큐','R': '알','S': '에스','T': '티',
                'U': '유', 'V': '브이','W': '더블유','X': '엑스','Y': '와이', 'Z': '제트'}
        target_list = eng.keys()
        for one in target_list:
            res = res.replace(one, eng[one])
        print('NUM : {} \n ==> KOR: {}'.format(text, res))
        print('CONVERT ENG LIST TO KOR end')
        return res

    # tts 리쿼스트 문장 정제
    # text : 스크립트
    # lang : 언어 (1:한국어, 2: 영어)
    def convert_script(self, text, lang=1):
        cp_txt = text
        pattern_with_end = None
        pattern_without_end = None
        try:
            if lang == 1:
                pattern_without_end = '([가-힇\s]+[^.!?~(\\n))]+$)'
                pattern_with_end = '([가-힇\s]+[.!?~(\\n))]+)'
            elif lang == 2:
                pattern_without_end = '([\w\s]+[^.!?~(\\n))]+$)'
                pattern_with_end = '([\w\s]+[.!?~(\\n))]+)'

            if None not in [pattern_with_end, pattern_without_end]:
                p = re.compile(pattern_without_end)
                p2 = re.compile(pattern_with_end)
                end_idx = -1

                for m in p.finditer(cp_txt):
                    end_idx = int(m.end())

                if end_idx > 0:
                    end_idx = int(m.end())
                    cp_txt = cp_txt[:end_idx] + '.' + cp_txt[end_idx:]

                rep = []
                for m in p2.finditer(cp_txt):
                    rep.append(int(m.end()))
                    # print(m.start(), m.end())
                rep.sort(reverse=True)
                txt_len = len(cp_txt)
                for r in rep:
                    print (r, txt_len)
                    if txt_len > r:
                        cp_txt = cp_txt[:r] + "|" + cp_txt[r:]
        except Exception as e:
            print ('convert_script failed {}'.format(e))
        finally:
            return cp_txt
