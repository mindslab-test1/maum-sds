#! /usr/bin/env python
#-*- coding: utf-8 -*-


import sys
from importlib import reload

reload(sys)

import os
import requests
import json
from voice_bot.ai_da_lib.num_to_text import stringTodigit

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(os.path.join(os.getenv('MAUM_ROOT'), 'lib/python'))
sys.path.append(os.path.realpath(sys.argv[0] + '/../../'))
sys.path.append(lib_path)

from voice_bot.config.config import BSConfig, DaConfig, HCallMysqlConfig
import websocket
from websocket import create_connection
ip = DaConfig.web_socket_ip
port = DaConfig.web_socket_port
websocket_url= 'ws://{0}:{1}/callsocket'.format(ip, port)
from voice_bot.ai_da_lib.dbconnector import DbConnector
from voice_bot.ai_check_info import ai_sql

sqls = ai_sql.SQLSET()
hc_mysql = DbConnector(HCallMysqlConfig)

## function name : send_socket
# task_seq : CM_CAMPAIGN_INFO 의 CAMPAIGN_INFO_ID
# is_success : 'Y' or 'N'
# task_info :task 명
# contract_no : contract_no
# camp_id: 캠페인 id
# call_id: 콜 id
def send_socket(task_seq, is_success, contract_no, camp_id, call_id):
    print('send socket')
    try:
        task_info = ''
        seq = ''
        print(sqls.TASK_INFO, camp_id, task_seq)
        row = hc_mysql.execute_with_params(sqls.TASK_INFO, (camp_id, task_seq))
        print(row)
        if row is not None:
            task_info = row[0]
            seq = row[1]
        print(sqls.UPDATE_DETECT, (call_id, contract_no, camp_id, task_seq, task_info, is_success))
        hc_mysql.execute_query_with_params(sqls.UPDATE_DETECT,
                                         (call_id, contract_no, camp_id, task_seq, task_info, is_success))
        ws = create_connection(websocket_url)
        data = dict()
        data['EventType'] = 'DETECT'
        data['Event'] = 'result'
        data['No'] = seq
        data['Result'] = is_success
        data['contract_no'] = contract_no
        ws.send(json.dumps(data))
        ws.close()
        print('[send_socket] END', websocket_url, data)
    except Exception as e:
        print('[send_socket] END Failed', e)


def _byteify(data, ignore_dicts = False):
    if isinstance(data, unicode):
        return data.encode('utf-8')
    if isinstance(data, list):
        return [ _byteify(item, ignore_dicts=True) for item in data ]
    if isinstance(data, dict) and not ignore_dicts:
        return {
            _byteify(key, ignore_dicts=True): _byteify(value, ignore_dicts=True)
            for key, value in data.iteritems()
        }
    return data

class BsLogic:
    host = ''
    port = ''
    logger = None

    def __init__(self, logger = None):
        self.logger = logger
        self.host = BSConfig.host
        self.port = BSConfig.port
        # self.logger.debug('[BsLogic]init end')

    def get_path(self, contractno, intent='', utter=''):
        print('==>get_path', contractno, intent, utter)
        path = ''
        cust_name = ''
        cust_id = ''
        camp_id = ''
        rep = ''
        headers = {
            'Content-Type': 'application/json',
            'cache-control': 'no-cache',
        }
        url = 'http://' + self.host + ':' + self.port + '/check/method'
        data_param = {'contractno': str(contractno), 'intent': str(intent)}
        response = requests.post(url, headers=headers, data=json.dumps(data_param))
        prod_name = ''
        month_fee = ''
        unpaid_year = ''
        unpaid_month = ''
        tot_fee = ''
        tot_month = ''
        cust_api_id = ''
        print('****', url)
        if response.ok:
            cont = response.content
            cv_res_js = json.loads(cont, object_hook=_byteify)
            # print '****', cv_res_js
            # print cv_res_js['answer']['answer']
            if 'path' in cv_res_js:
                path = cv_res_js['path']
            if 'cust_name' in cv_res_js:
                cust_name = cv_res_js['cust_name']
            if 'camp_id' in cv_res_js:
                camp_id = cv_res_js['camp_id']
            if 'cust_id' in cv_res_js:
                cust_id = cv_res_js['cust_id'] if cv_res_js['cust_id'] is not None else ''
        print('[get_path] path: {}, name: {}, camp: {}, cust_id: {}'.format(path, cust_name, camp_id, cust_id))
        if path != '' and path is not None:
            # path에따라 체크 로직 수행
            url2 = 'http://' + self.host + ':' + self.port + path
            print('***', url2)
            data_param2 = {'contractno': contractno, 'intent': intent, 'utter': utter, 'campaign_id': camp_id, 'cust_id': cust_id}
            response2 = requests.post(url2, headers=headers, data=json.dumps(data_param2))
            if response2.ok:
                cont2 = response2.content
                cv_res_js2 = json.loads(cont2, object_hook=_byteify)
                print('****', cv_res_js2)
                if 'rep' in cv_res_js2:
                    rep = cv_res_js2['rep']
                if 'prod_name' in cv_res_js2:
                    prod_name = stringTodigit(str(cv_res_js2['prod_name']))
                if 'month_fee' in cv_res_js2:
                    month_fee = stringTodigit(str(cv_res_js2['month_fee']))
                if 'tot_fee' in cv_res_js2:
                    tot_fee = stringTodigit(str(cv_res_js2['tot_fee']))
                if 'unpaid_year' in cv_res_js2:
                    unpaid_year = stringTodigit(str(cv_res_js2['unpaid_year']))
                if 'unpaid_month' in cv_res_js2:
                    unpaid_month = stringTodigit(str(cv_res_js2['unpaid_month']))
                if 'tot_month' in cv_res_js2:
                    tot_month = stringTodigit(str(cv_res_js2['tot_month']))
                if 'cust_api_id' in cv_res_js2:
                    cust_api_id = cv_res_js2['cust_api_id']
        m_list = [prod_name, month_fee, tot_fee, unpaid_year, unpaid_month, tot_month, cust_api_id]
        print(
        '[get_path] END path : {}, name: {}, camp: {}, rep: {}, m_list: {}'.format(path,
                                                                                  cust_name,
                                                                                  camp_id,
                                                                                  rep, m_list))
        return path, cust_name, rep, camp_id, m_list

    def get_cust_nm(self, contractno):
        result = { 'cust_name': '', 'cust_id': '', 'camp_id': '' }
        headers = {'Content-Type': 'application/json','cache-control': 'no-cache'}
        url = 'http://' + self.host + ':' + self.port + '/get/cust_name'
        data_param = {'contractno': str(contractno)}
        response = requests.post(url, headers=headers, data=json.dumps(data_param))
        if response.ok:
            cont = response.content
            cv_res_js = json.loads(cont, object_hook=_byteify)
            if 'cust_name' in cv_res_js:
                result['cust_name'] = cv_res_js['cust_name']
            if 'campaign_id' in cv_res_js:
                result['camp_id'] = cv_res_js['campaign_id']
            if 'cust_id' in cv_res_js:
                result['cust_id'] = cv_res_js['cust_id']
        return result

    def save_history(self, contractno, intent=''):
        headers = {
            'Content-Type': 'application/json',
            'cache-control': 'no-cache',
        }
        url = 'http://' + self.host + ':' + self.port + '/save/history'
        data_param = {'contractno': str(contractno), 'intent': str(intent)}
        response = requests.post(url, headers=headers, data=json.dumps(data_param))
        print('****', url)
        if response.ok:
            return False
        else:
            return True

    def get_cust_list(self, contractno=''):
        headers = {
            'Content-Type': 'application/json',
            'cache-control': 'no-cache',
        }
        url = 'http://' + self.host + ':' + self.port + '/get/custList'
        data_param = {'contractno': str(contractno)}
        response = requests.post(url, headers=headers, data=json.dumps(data_param))
        print('****', url)
        if response.ok:
            cont = response.content
            cv_res_js = json.loads(cont, object_hook=_byteify)
        print('[get_cust_list] END res: {}'.format(cv_res_js))
        return cv_res_js

    def get_simple_bot_info(self, contractno=''):
        result = {
            'sds_url' : '',
            'sds_host' : '',
            'campaign' : '',
            'cust_name' : '',
            'lang' : ''
        }

        headers = {
           'Content-Type': 'application/json',
           'cache-control': 'no-cache',
        }
        url = 'http://' + self.host + ':' + self.port + '/get/sdsInfo'
        data_param = {'contractno': str(contractno)}
        response = requests.post(url, headers=headers, data=json.dumps(data_param))
        print('****', url)
        if response.ok:
            cont = response.content
            cv_res_js = json.loads(cont, object_hook=_byteify)
            if 'sds_url' in cv_res_js:
                result['sds_url'] = cv_res_js['sds_url']
            if 'sds_host' in cv_res_js:
                result['sds_host'] = cv_res_js['sds_host']
            if 'campaign' in cv_res_js:
                result['campaign'] = cv_res_js['campaign']
            if 'cust_name' in cv_res_js:
                result['cust_name'] = cv_res_js['cust_name']
            if 'lang' in cv_res_js:
                result['lang'] = cv_res_js['lang']
        print('[get_simple_bot_info] END res: {}'.format(result))
        return result

    def send_simiple_socket(self, svc_host='', talk = '', user = ''):
        result = {
            'status' : '',
            'message' : ''
        }

        headers = {
           'Content-Type': 'application/json',
           'cache-control': 'no-cache',
        }
        url = 'http://' + self.host + ':' + self.port + '/send/socket'
        data_param = {'host': str(svc_host), 'msg': talk, 'user': user}
        response = requests.post(url, headers=headers, data=json.dumps(data_param))
        print('****', url)
        if response.ok:
            cont = response.content
            cv_res_js = json.loads(cont, object_hook=_byteify)
            if 'status' in cv_res_js:
                result['status'] = cv_res_js['status']
            if 'message' in cv_res_js:
                result['message'] = cv_res_js['message']
        print('[get_simple_bot_info] END res: {}'.format(result))
        return result

    def next_func(self, contractno='', intent='', camp_id='', callid = ''):

        # 다음 응답 준비
        stat = ''
        headers = {
            'Content-Type': 'application/json',
            'cache-control': 'no-cache',
        }
        url = 'http://' + self.host + ':' + self.port + '/check/func'
        data_param = {'contractno': contractno, 'intent': intent, 'campaign_id': camp_id}
        print('****', url, data_param)
        response = requests.post(url, headers=headers, data=json.dumps(data_param))
        socket_task_seq = None
        detect_type = None
        if response.ok:
            cont = response.content
            cv_res_js = json.loads(cont, object_hook=_byteify)
            # print cv_res_js
            # print cv_res_js['answer']['answer']
            if 'ter' in cv_res_js:
                stat = cv_res_js['ter']
            if 'socket_task_seq' in cv_res_js:
                socket_task_seq = cv_res_js['socket_task_seq']
            if 'detect_type' in cv_res_js:
                detect_type = cv_res_js['detect_type']
            # 탐지정보 업데이트
            if socket_task_seq is not None and socket_task_seq != '':
                if detect_type is not None and detect_type != '':
                    print(socket_task_seq, detect_type, contractno, camp_id, callid)
                    send_socket(socket_task_seq, detect_type, contractno, camp_id, callid)

        self.logger.debug('[next_func] ter: {}'.format(stat))
        return stat

    def next_simple_func(self, contractno='', intent='', camp_id='', callid = ''):

        # 다음 응답 준비
        stat = ''
        headers = {
            'Content-Type': 'application/json',
            'cache-control': 'no-cache',
        }
        url = 'http://' + self.host + ':' + self.port + '/check/func'
        data_param = {'contractno': contractno, 'intent': intent, 'campaign_id': camp_id}
        print('****', url, data_param)
        response = requests.post(url, headers=headers, data=json.dumps(data_param))
        if response.ok:
            cont = response.content
            cv_res_js = json.loads(cont, object_hook=_byteify)
            if 'ter' in cv_res_js:
                stat = cv_res_js['ter']
            # 탐지정보 업데이트
            # if socket_task_seq is not None and socket_task_seq != '':
            #     if detect_type is not None and detect_type != '':
            #         print socket_task_seq, detect_type, contractno, camp_id, callid
            #         send_socket(socket_task_seq, detect_type, contractno, camp_id, callid)

        self.logger.debug('[next_simple_func] ter: {}'.format(stat))
        return stat

if __name__ == '__main__':
    bs = BsLogic()
    bs.get_path('104', '개인정보확인', '구공공일공일', '')
    # print exe_path, os.path.realpath(sys.argv[0] + '/../../')
