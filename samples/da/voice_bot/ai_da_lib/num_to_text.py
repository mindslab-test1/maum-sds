# -*- coding:utf-8 -*-

'''
https://wyseburn.tistory.com/entry/%EC%88%AB%EC%9E%90%EB%A5%BC-%ED%95%9C%EA%B8%80-%EC%9B%90-%EB%8B%A8%EC%9C%84%EB%A1%9C-%EB%B3%80%EA%B2%BD%ED%95%98%EB%8A%94-%ED%95%A8%EC%88%98
https://lightblog.tistory.com/71
모듈명 : krw_to_korean_won
작성자 : 서성현
작성일 : 2020.02.27
설명 : 숫자를 입력했을때, 숫자+한글 금액으로 변환
셈플
    print(krw_to_korean_won('1234567890')) -> 12억 3456만 7890원
    print(krw_to_korean_won('1,234,567,890')) -> 12억 3456만 7890원
'''

import re

def krw_to_korean_won(arg):
    amount = arg.replace(',', '')
    if int(amount) > 99999999:
        # print amount[0:-8],'억',amount[-8:-4],'만',amount[-4:], '원'
        return '{0}억 {1}만 {2}원'.format(amount[0:-8], amount[-8:-4], amount[-4:])
    elif int(amount) > 9999:
        # print amount[-8:-4],'만',amount[-4:], '원'
        return '{0}만 {1}원'.format(amount[-8:-4], amount[-4:])
    else:
        # print amount[-4:],'원'
        return '{0}원'.format(amount[-4:])


'''
https://m.blog.naver.com/PostView.nhn?blogId=chandong83&logNo=221144077279&proxyReferer=https%3A%2F%2Fwww.google.com%2F
모듈명 : digit2txt
작성자 : 서성현
작성일 : 2020.02.27
설명 : 숫자를 입력했을때, 한글로 변환
셈플
    print(digit2txt(str(1234)))
     -> 천이백삼십사
'''

def digit2txt(strNum):
    # 만 단위 자릿수
    tenThousandPos = 4
    # 억 단위 자릿수
    hundredMillionPos = 9
    txtDigit = ['', '십', '백', '천', '만', '억']
    txtNumber = ['', '일', '이', '삼', '사', '오', '육', '칠', '팔', '구']
    txtPoint = '쩜 '

    resultStr = ''
    digitCount = 0
    # print(strNum)
    # 자릿수 카운트
    for ch in strNum:
        # ',' 무시
        if ch == ',':
            continue
        #소숫점 까지
        elif ch == '.':
            break
        digitCount = digitCount + 1

    digitCount = digitCount-1
    index = 0

    while True:
        notShowDigit = False
        ch = strNum[index]
        # print(str(index) + ' ' + ch + ' ' +str(digitCount))
        # ',' 무시
        if ch == ',':
            index = index + 1
            if index >= len(strNum):
                break;
            continue

        if ch == '.':
            resultStr = resultStr + txtPoint
        else:
            # 자릿수가 2자리이고 1이면 '일'은 표시 안함.
            # 단 '만' '억'에서는 표시 함
            if(digitCount > 1) and (digitCount != tenThousandPos) and  (digitCount != hundredMillionPos) and int(ch) == 1:
                resultStr = resultStr + ''
            elif int(ch) == 0:
                resultStr = resultStr + ''
                # 단 '만' '억'에서는 표시 함
                if (digitCount != tenThousandPos) and  (digitCount != hundredMillionPos):
                    notShowDigit = True
            else:
                resultStr = resultStr + txtNumber[int(ch)]

        # 1억 이상
        if digitCount > hundredMillionPos:
            if not notShowDigit:
                resultStr = resultStr + txtDigit[digitCount-hundredMillionPos]
        # 1만 이상
        elif digitCount > tenThousandPos:
            if not notShowDigit:
                resultStr = resultStr + txtDigit[digitCount-tenThousandPos]
        else:
            if not notShowDigit:
                resultStr = resultStr + txtDigit[digitCount]

        if digitCount <= 0:
            digitCount = 0
        else:
            digitCount = digitCount - 1
        index = index + 1
        if index >= len(strNum):
            break
    return resultStr

'''
모듈명 : stringTodigit
작성자 : 황민혜
작성일 : 2020.03.02
설명 : 한글 문자열에서 숫자만 추출해 한글화
셈플
    print(stringTodigit('100세 보험'))
     -> 천이백삼십사
'''

def stringTodigit(words):
    m = re.compile('\d+')
    la = m.findall(words)
    la = list(set(la))
    la.sort(key=int, reverse=True)
    la_cv = list(map(lambda i: digit2txt(i), la))
    dictionary = dict(zip(la, la_cv))
    print(dictionary)
    for key in dictionary.keys():
        words = words.replace(key, dictionary[key])
    print(words)
    print(la)
    return words

if __name__ == "__main__":
    # print(krw_to_korean_won('1234567890'))
    # print(krw_to_korean_won('1,234,567,890'))
    # print(digit2txt(str(110)))
    print(digit2txt(str(1234)))
    stringTodigit(str('100세 보험'))
    stringTodigit('100세 보험')
