#! /usr/bin/env python
#-*- coding: utf-8 -*-

# imports
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import os
import grpc
import argparse

from concurrent import futures
import time

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(os.path.join(os.getenv('MAUM_ROOT'), 'lib/python'))
sys.path.append(lib_path)

from google.protobuf import empty_pb2
from maum.brain.sds import resolver_pb2
from maum.brain.sds import resolver_pb2_grpc
from maum.brain.sds import model_manager_pb2_grpc
from maum.brain.sds import sds_pb2
from maum.brain.sds import sds_pb2_grpc
from voice_bot.config.config import SdsConfig
# Dialog method

class SDS:
    sds_stub = None
    resolver_stub = None
    model_manager_stub = None
    sds_channel = None
    channel = None

    def __init__(self, logger = None, model = None, lang = None, is_external = None):
        print('[sds]init start')
        # self.channel = None
        model_list = [model]
        self.logger = logger
        self.logger.debug('[sds]init end')
        add_info = SdsConfig.host + ':' + str(SdsConfig.port)
        print(add_info)
        self.sds_channel = grpc.insecure_channel(add_info)
        print(self.sds_channel)
        self.resolver_stub = resolver_pb2_grpc.SdsServiceResolverStub(self.sds_channel)
        self.model_manager_stub = model_manager_pb2_grpc.SdsModelManagerStub(self.sds_channel)

        MG = resolver_pb2.ModelGroup()
        MG.name = model
        MG.lang = lang
        MG.is_external = is_external

        try:
            self.model_manager_stub.CreateModelGroup(MG)
        except Exception as e:
            print("Model group already exist.")

        MD = resolver_pb2.Model()
        MD.lang = MG.lang
        MD.is_external = MG.is_external

        MP= resolver_pb2.ModelParam()
        MP.lang = MG.lang
        MP.is_external = MG.is_external
        MP.group_name = MG.name


        ML = resolver_pb2.ModelList()
        for mn in model_list:
            MD.name = mn
            MP.model_name = mn
            Model = ML.models.add()
            self.resolver_stub.LinkModel(MP)

        print('MG : ', MG)
        self.logger.debug('[SDS GetServer] Find')
        server_status = self.resolver_stub.Find(MG)
        print('[find_result]')
        print(server_status)

        if server_status.state is resolver_pb2.SERVER_STATE_STARTING:
            print("SERVER IS STARTING.")
            time.sleep(5)

        sds_server_addr = server_status.server_address
        #self.sds_server_addr = server_status.server_address
        print(sds_server_addr)
        self.channel = grpc.insecure_channel(server_status.server_address)
        grpc.channel_ready_future(self.channel).result(timeout=10)

        # self.channel = None
        # self.logger = logger
        # self.logger.debug('[sds]init end')
        # add_info = SdsConfig.host + ':' + str(SdsConfig.port)
        # self.sds_channel = grpc.insecure_channel(add_info)
        # self.resolver_stub = resolver_pb2_grpc.SdsServiceResolverStub(self.sds_channel)
        # self.model_manager_stub = model_manager_pb2_grpc.SdsModelManagerStub(self.sds_channel)

        #self.resolver_stub = None
        # self.sds_stub = None
        #self.model_manager_stub = None

    # SDS Œ­¹ö œÇÇà
    def GetServer(self, model, lang, is_external, session_id):
        self.logger.debug('====[SDS GetServer] start ====')
        ## add_info = SdsConfig.host + ':' + str(SdsConfig.port)
        grpc.channel_ready_future(self.channel).result(timeout=10)

        self.logger.debug('self.channel : {}'.format(self.channel))
        self.sds_stub = sds_pb2_grpc.SpokenDialogServiceStub(self.channel)
        #self.sds_stub = sds_pb2_grpc.SpokenDialogServiceStub(grpc.insecure_channel(server_status.server_address))
        empty = empty_pb2.Empty()
        self.logger.debug('==stub: {}'.format(self.sds_stub))
        print('make stub done')
        # CURRENT_ML = sds_stub.GetCurrentModels(empty)
        #print '[CURRENT_ML]'
        #print CURRENT_ML

        # AVAIL_ML = sds_stub.GetAvailableModels(empty))
        # print '[AVAILABLE_ML]'
        #print AVAIL_ML

        # pong = self.sds_stub.Ping(MG, new CallOptions().WithWaitForReady(true))
        # pong = self.sds_stub.Ping(MG, wait_for_ready=True)
        # pong = self.sds_stub.Ping(MG)
        # print pong

        dialog_param = sds_pb2.DialogueParam()
        dialog_param.model = model
        dialog_param.session_key = session_id
        dialog_param.user_initiative = False
        print('dialog_param')
        self.logger.debug('>>>>>>>>>>>>dialog_param : {}'.format(dialog_param))
        #print dialog_param

        #openResult = self.sds_stub.Open(dialog_param, new CallOptions().WithWaitForReady(true))
        try:
            openResult = self.sds_stub.Open(dialog_param)
        except Exception as e:
            self.logger.debug('<<<<<<<<<<<<<<<<<<< : {}'.format(str(e)))
        #print '[OpenResult]'
        #print openResult
        self.logger.debug('====[SDS GetServer] OpenResult : {}'.format(openResult))
        self.logger.debug('====[SDS GetServer] end ====')
        print('openResult', openResult.sds_response.response)
        return openResult

    def Talk(self, text, session_id, model, lang, is_external, indri_score=0, pre_intent = ""):
        self.logger.debug('====[SDS Talk] start ====')
        # print '#@$U(@#$(@#&(@#$&@#&(@#$&(', repeat_cnt
        #self.GetServer(model, lang, is_external, session_id)
        empty = empty_pb2.Empty()
        sds_query = sds_pb2.SdsQuery()
        sds_query.model = model
        sds_query.session_key = session_id
        sds_query.utter = text.encode('UTF-8')

        sds_utter = sds_pb2.SdsUtter()
        self.logger.debug('sds_qery : {}'.format(sds_query))
        under_intent = ''
        print('*****encoding', sys.stdin.encoding)
        try :
            grpc.channel_ready_future(self.channel).result(timeout=10)
            intent = self.sds_stub.Understand(sds_query)
            under_intent = intent.intent
            # if str(under_intent) == "unknown":
            #     self.logger.debug('change_utter')
            #     sds_query.utter = "¹¬Àœ"
            #     intent = self.sds_stub.Understand(sds_query)
            #     under_intent = intent.intent
            # #print '[intent]'
            #print intent
            self.logger.debug('#utter {} - #intent : {}'.format(sds_query.utter, intent.intent))
            print(type(intent.intent))
            print(intent)
            self.logger.debug('#pre_sys_intent : {}'.format(pre_intent))
            entities = sds_pb2.Entities()
            entities.session_key = session_id
            entities.model = model
            entities.reset_db = True
            data = sds_pb2.EntityData()
            if intent.filled_entities:
                self.logger.debug(intent.filled_entities)
                for i_entities in intent.filled_entities.keys():
                    self.logger.debug(i_entities)
                    data.entities[i_entities] = intent.filled_entities[i_entities]

                self.logger.debug("1")
            self.logger.debug("2")
            # if(1) :
                # print '****re_cnt',  str(repeat_cnt)
                #data.entities['REPEAT_COUNT'.encode("utf-8")] = str(repeat_cnt).encode("utf-8")
            # print '****data.entities', data.entities
            # if str(intent.intent) == "inform" and num_val != "":
            #     data.entities['NUM'.encode("utf-8")] = num_val.encode("utf-8")
            #     data.entities['REPEAT_COUNT'.encode("utf-8")] = str(repeat_cnt).encode("utf-8")
            # elif num_val != 'ÀÏ':
            #     data.entities['REPEAT_COUNT'.encode("utf-8")] = str(repeat_cnt).encode("utf-8")

            # data.entities['REPEAT_COUNT'.encode("utf-8")] = str(repeat_cnt).encode("utf-8")
            # self.logger.debug('Understand data entities : {}'. data)
            entity_list = ['country','people','sym','group','temp']

            if intent.intent == "want" or intent.intent == "interview":
                self.logger.debug("3")

                if data.entities['country'.encode("utf-8")] != "":
                    check_cnty = "Y"
                    data.entities['CHECK_CNTY'.encode("utf-8")] = str(check_cnty).encode("utf-8")

                if data.entities['people'.encode("utf-8")] != "":
                    check_pple = "Y"
                    data.entities['CHECK_PPLE'.encode("utf-8")] = str(check_pple).encode("utf-8")

                if data.entities['sym'.encode("utf-8")] != "":
                    check_sym = "Y"
                    data.entities['CHECK_SYM'.encode("utf-8")] = str(check_sym).encode("utf-8")

                if data.entities['group'.encode("utf-8")] != "":
                    check_group = "Y"
                    data.entities['CHECK_GROUP'.encode("utf-8")] = str(check_group).encode("utf-8")

                if data.entities['temp'.encode("utf-8")] != "":
                    check_temp = "Y"
                    data.entities['CHECK_TEMP'.encode("utf-8")] = str(check_temp).encode("utf-8")

            self.logger.debug("4")
            if intent.intent == "check_info":
                # if check_cnty == '' or check_pple == '' or check_sym == '' or check_group =='' or check_temp =='':
                print(3)
                data.entities['CHECK_INFO'.encode("utf-8")] = str("Y").encode("utf-8")

            if pre_intent == "req_visit_country": # 이전에 해외방문여부 물어보고,
                if intent.intent == "affirm": # 대답을 Yes로 할 경우
                    check_cnty = "Y"
                else:
                    check_cnty = "N"
                data.entities['CHECK_CNTY'.encode("utf-8")] = str(check_cnty).encode("utf-8")

            if pre_intent == "req_around_searching":  # 이전에 주변확진여부 물어보고,
                if intent.intent == "affirm": # 대답을 Yes로 할 경우
                    check_pple = "Y"
                else:
                    check_pple = "N"
                data.entities['CHECK_PPLE'.encode("utf-8")] = str(check_pple).encode("utf-8")

            if pre_intent == "req_specical_group": # 이전에 특정 집단과 접촉여부 물어보고,
                if intent.intent == "affirm": # 대답을 Yes로 할 경우
                    check_group = "Y"
                else:
                    check_group = "N"
                data.entities['CHECK_GROUP'.encode("utf-8")] = str(check_group).encode("utf-8")

            if pre_intent == "req_high_temp": # 이전에 발열여부 물어보고,
                if intent.intent == "affirm": # 대답을 Yes로 할 경우
                    check_temp = "Y"
                else:
                    check_temp = "N"
                data.entities['CHECK_TEMP'.encode("utf-8")] = str(check_temp).encode("utf-8")

            if pre_intent == "req_another_symp": # 이전에 다른 기타증상여부 물어보고,
                if intent.intent == "affirm": # 대답을 Yes로 할 경우
                    check_sym = "Y"
                else:
                    check_sym = "N"
                data.entities['CHECK_SYM'.encode("utf-8")] = str(check_sym).encode("utf-8")

            self.logger.debug('#######data.entities : {}'.format(data.entities))
            entities.entity_data.extend([data])

            grpc.channel_ready_future(self.channel).result(timeout=10)
            sds_utter = self.sds_stub.Generate(entities)
            self.logger.debug('#######entities : {}'.format(entities))
            #print sds_utter
            #print '[sds utter response]'
            #print sds_utter.response
            #print '====[Talk] end ===='
        except Exception as ex:
            print (ex)
            self.logger.error(ex)
            self.logger.error('====[SDS Talk] Cannot get sds response===')
        self.logger.debug('====[SDS Talk] end ====')
        # self.logger.debug('sds_finished : {}'. sds_utter.finishd)
        # self.logger.debug('sds_utter : {}'. sds_utter.response)

        return sds_utter, under_intent

if __name__ == '__main__':
    #SDS = SDS()
    Sds = SDS()
    print('[main] start')
    sds_res = self.Sds.GetServer(SdsConfig.model, SdsConfig.lang, SdsConfig.is_external, talk.session.id)
    print(sds_res)
    sds_t_res = Sds.Talk(SdsConfig.model, SdsConfig.lang, SdsConfig.is_external)
    print(sds_t_res)
    print('[main] end')
