#!/usr/bin/python
# -*- coding: utf-8 -*-

# imports
import os
import sys

class SQLSET:
    ## 2Â÷ ÄÝ ³¯ž±¶§. Á¶°Ç(20000¹øŽë ÀÌ»ó)
    COND_SECOND_CM = 'and CONTRACT_NO >= 20000 '
    COND_SECOND = 'and CUST_ID >= 20000 '
    MSG_CODE_TABLE = 'MSG_LIST_MGNT'
    CUSTOM_TABLE = 'CUSTOM_LIST_AI'# 'TBC_CAMP_LIST_AI'
    CONTRACT_TABLE = 'CM_CONTRACT'
    CM_CUST_TABLE = 'CUST_BASE_INFO'
    PRD_TABLE = 'PRD_MGNT'
    CUSTOM_INFO_WITH_INSUR_NO_AND_CUSTOM_ID = 'select * from ' + CUSTOM_TABLE + ' where INSUR_NO=%s and NK_CUST_ID=%s'
    CUSTOM_NAME_BY_TELL = 'select CUST_NAME from ' + CUSTOM_TABLE + ' where CELLULAR_NO=%s '
    CUSTOM_INFO_BY_TELL = 'select * from ' + CUSTOM_TABLE + ' where CELLULAR_NO=%s'

    CUSTOM_SUMMARY_INFO_WITH_INSUR_NO_AND_CUSTOM_ID = '''select NK_CUST_ID, CUST_NAME, left(SOCIAL_ID, 6) as BIRTH,
                                                        INSUR_NO, ETC_COL_5 from ''' + CUSTOM_TABLE + '''
                                                        where  CELLULAR_NO=%s '''

    CUSTOM_ALL_INFO_BY_TELL = '''select *, left(SOCIAL_ID, 6) as BIRTH from ''' + CUSTOM_TABLE + ''' where  CELLULAR_NO=%s '''

    MSG_CODE_INFO = 'select TEXT, MSG_TYPE from ' + MSG_CODE_TABLE + ' where use_yn = "Y" and MSG_CD in '
    MSG_CODE_INFO_EACH = 'select TEXT, MSG_TYPE from ' + MSG_CODE_TABLE + ' where use_yn = "Y" and MSG_CD = %s '
    PHONE_NO_BY_CTT_NO = 'select TEL_NO from ' + CONTRACT_TABLE + ' where CONTRACT_NO = %s '
    UPDATE_CALL_YN = 'update ' + CM_CUST_TABLE + ' set CERTIFI_NO = "Y", MODIFY_DT=now()  where CUST_TEL_NO = %s ' + COND_SECOND
    UPDATE_CALL_YN2 = 'update ' + CM_CUST_TABLE + ' set CERTIFI_NO = "Y", MODIFY_DT=now() where CUST_TEL_NO = %s '
    UPDATE_CALL_MOD = 'update ' + CM_CUST_TABLE + ' set MODIFY_DT=now() where CUST_TEL_NO = %s ' + COND_SECOND
    UPDATE_CREATE_DT = 'update ' + CM_CUST_TABLE + ' set CREATE_DT=now() where CUST_ID = %s ' + COND_SECOND
    SUCCESS_CALL_YN = 'update ' + CM_CUST_TABLE + ' set TEL_COMP_SAVE_YN = "Y" where CUST_ID = %s ' + COND_SECOND

    #SUCCESS_CALL_YN = 'update ' + CM_CUST_TABLE + ' set TEL_COMP_SAVE_YN = "Y" where CUST_TEL_NO = %s '
    CONTRACT_NO_BY_TEL_NO = 'select a.CONTRACT_NO from ' + CONTRACT_TABLE + ' as a inner join ' + CM_CUST_TABLE + ' as b ON a.TEL_NO = b.CUST_TEL_NO where b.TEL_COMP_SAVE_YN = "N" order by JUMIN_NO DESC'
    CONTRACT_NO_BY_TEL_NO2 = 'select a.CONTRACT_NO as CONTRACT_NO, a.TEL_NO as TEL_NO from ' + CONTRACT_TABLE + ' as a inner join ' + CM_CUST_TABLE + ' as b ON a.TEL_NO = b.CUST_TEL_NO where b.TEL_COMP_SAVE_YN = "N" order by JUMIN_NO DESC'
    UTTER_YN_TYPE = 'select YN_TYPE from REPLY_Y_N_SET where replace(TEXT,\' \',\'\') = replace(%s, \' \',\'\')'


    CUSTOM_SUMMARY_INFO_BY_TELL = '''select NK_CUST_ID, CUST_NAME, left(SOCIAL_ID, 6) as BIRTH, ETC_COL_3, PRD_MGNT.INKD_RE_PL_NAME_VOICE AS ETC_COL_5, ETC_COL_6, ETC_COL_10,CELLULAR_NO, SOCIAL_ID,OFFICE, HEAD_OFFICE, INSUR_NO, if(year(now())-(if(mid(SOCIAL_ID ,7,1) ='1' or mid(SOCIAL_ID ,7,1) ='2' or mid(SOCIAL_ID,7,1)='5' or mid(SOCIAL_ID ,7,1)='6', 1900, 2000) + LEFT(SOCIAL_ID,2) + 1) > 59, 'Y', 'N') as AGE_YN from ''' + CUSTOM_TABLE + '''
                                                        LEFT JOIN ''' + PRD_TABLE + '''
                                                        ON PRD_MGNT.INKD_BSNS_NAME = TBC_CAMP_LIST_AI.ETC_COL_5
                                                        where  CELLULAR_NO=%s '''


    #PRD_COUNT_IS_ONE = 'select LIST_SEQ_NO as CONTRACT_NO from TBC_CAMP_LIST_AI where LIST_SEQ_NO = %s and ETC_COL_1 = 1;'
    PRD_COUNT_IS_ONE = 'select LIST_SEQ_NO as CONTRACT_NO, INSUR_NO from TBC_CAMP_LIST_AI where LIST_SEQ_NO = %s and ETC_COL_1 = 1;'
    # CONTRACT_NO_BY_TEL_NO4 = 'select a.CONTRACT_NO from ' + CONTRACT_TABLE + ' as a inner join ' + CM_CUST_TABLE + ' as b ON a.TEL_NO = b.CUST_TEL_NO where b.TEL_COMP_SAVE_YN = "Y" and b.CERTIFI_NO = "N" order by b.JUMIN_NO DESC, a.CONTRACT_NO limit 300'

    # Æò»óœÃ
    CONTRACT_NO_BY_TEL_NO6 = 'select CONTRACT_NO from CM_CONTRACT a, (  select distinct(CUST_TEL_NO), JUMIN_NO  from CUST_BASE_INFO     where TEL_COMP_SAVE_YN="N") b where a.TEL_NO = b.CUST_TEL_NO and a.LAST_CALL_ID IS NULL order by b.JUMIN_NO DESC, a.CONTRACT_NO limit 510;'

    # ÀüÈ­ ÇÑ¹øµµ ŸÈÇÑ »ç¶÷µé Žë»óÀž·Î ³¯ž®ŽÂ Äõž®
    CONTRACT_NO_BY_TEL_NO6_1 = 'select CONTRACT_NO from CM_CONTRACT a,(select CUST_TEL_NO, JUMIN_NO from CUST_BASE_INFO ) b where a.TEL_NO = b.CUST_TEL_NO and a.LAST_CALL_ID IS NULL order by b.JUMIN_NO DESC, a.CONTRACT_NO;'

    CONTRACT_NO_BY_TEL_NO6_2 = 'select CONTRACT_NO from CM_CONTRACT a,(select CUST_TEL_NO, JUMIN_NO from CUST_BASE_INFO ) b where a.TEL_NO = b.CUST_TEL_NO limit 1000;'

    CONTRACT_NO_BY_TEL_NO7 = 'select CONTRACT_NO from CM_CONTRACT a, (  select distinct(CUST_TEL_NO), JUMIN_NO  from CUST_BASE_INFO     where TEL_COMP_SAVE_YN="Y" and CERTIFI_NO = "N" ) b where a.TEL_NO = b.CUST_TEL_NO order by b.JUMIN_NO DESC, a.CONTRACT_NO limit 510;'

    # ÀüÈ­ Çß°í, 1¹ø œÇÆÐÇÑ »ç¶÷µé
    CONTRACT_NO_BY_TEL_NO8 = 'select CONTRACT_NO from CM_CONTRACT a, (  select distinct(CUST_TEL_NO), JUMIN_NO  from CUST_BASE_INFO     where TEL_COMP_SAVE_YN="Y" and CERTIFI_NO = "N") b where a.TEL_NO = b.CUST_TEL_NO and a.CALL_TRY_COUNT=1 order by b.JUMIN_NO DESC, a.CONTRACT_NO'

    # ¿¬·Éº° žíŒö ±žÇÏŽÂ Äõž®
    GET_AGE_NO1 = '''select concat(age, '') as AGE1, count(age) as CNT from ( select CONTRACT_NO, floor((year(now())-(if(mid(JUMIN_NO,7,1) ='1' or mid(JUMIN_NO,7,1) ='2' or mid(JUMIN_NO,7,1)='5' or mid(JUMIN_NO,7,1)='6', 1900,2000) + LEFT(JUMIN_NO,2)) + 1)/10)*10 as age from (select CONTRACT_NO, JUMIN_NO from CM_CONTRACT a, (  select distinct(CUST_TEL_NO), JUMIN_NO from CUST_BASE_INFO     where TEL_COMP_SAVE_YN="N" and CERTIFI_NO = "N") b where a.TEL_NO = b.CUST_TEL_NO and
    CONTRACT_NO >= 20000
    and  a.CALL_TRY_COUNT=0
    order by b.JUMIN_NO DESC, a.CONTRACT_NO) c) as t group by age;
    '''
    # ¿¬·ÉŽëº° ÁŠÇÑ°³Œö¿¡ µû¶ó °í°Ž Á€ºž°¡Á®¿ÀŽÂ Äõž®
    GET_CUSTOM_BY_AGE ='''select concat(age, '') as age1, CONTRACT_NO from ( select CONTRACT_NO, floor((year(now())-(if(mid(JUMIN_NO,7,1) ='1' or mid(JUMIN_NO,7,1) ='2' or mid(JUMIN_NO,7,1)='5' or mid(JUMIN_NO,7,1)='6', 1900,2000) + LEFT(JUMIN_NO,2)) + 1)/10)*10 as age from (select CONTRACT_NO, JUMIN_NO from CM_CONTRACT a, (  select distinct(CUST_TEL_NO), JUMIN_NO from CUST_BASE_INFO     where TEL_COMP_SAVE_YN="N" and CERTIFI_NO = "N") b where a.TEL_NO = b.CUST_TEL_NO and
      CONTRACT_NO >= 20000
      and  a.CALL_TRY_COUNT=0
 order by b.JUMIN_NO DESC, a.CONTRACT_NO) c) as t where concat(age,'')=%s'''
    # È÷œºÅäž® œ×ŽÂ Äõž®
    STORE_HISTORY = '''INSERT INTO CALL_HISTORY(CONTRACT_NO,CUST_ID,CUST_NM,START_TIME,END_TIME,CALL_TIME,STATUS,STATUS_DET,DIAL_NO,ETC,LAST_MSG)
VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) '''

    UPDATE_CALL_HISTORY = '''UPDATE CALL_HISTORY SET SCENARIO_RESULT=%s WHERE CONTRACT_NO=%s ORDER BY CALL_ID DESC LIMIT 1'''

    # ÀüÈ­ °ÅÀý °í°Ž, Œöµ¿ TRY COUNT Áõ°¡ Äõž®
    UPDATE_TRY_CNT_FOR_DENY = '''update CM_CONTRACT set CALL_TRY_COUNT = CALL_TRY_COUNT + 1 where CONTRACT_NO=%s ''' + COND_SECOND_CM

    # check age
    BIRTH_CHECK = '''select CUST_ID, year(now())-(if(mid(JUMIN_NO,7,1) ='1' or mid(JUMIN_NO,7,1) ='2' or mid(JUMIN_NO,7,1)='5' or mid(JUMIN_NO,7,1)='6', 1900,2000) + LEFT(JUMIN_NO,2)) + 1 as age from CUST_BASE_INFO where floor((year(now())-(if(mid(JUMIN_NO,7,1) ='1' or mid(JUMIN_NO,7,1) ='2' or mid(JUMIN_NO,7,1)='5' or  mid(JUMIN_NO,7,1)='6', 1900,2000) + LEFT(JUMIN_NO,2)) + 1)/10)*10 = 10 and CUST_ID=%s;'''

    # get insur_no
    GET_INSUR_NO = 'select INSUR_NO from TBC_CAMP_LIST_AI where CELLULAR_NO=%s;'
    GET_INSUR_NO_BY_NO = 'select LIST_SEQ_NO as CONTRACT_NO, INSUR_NO from TBC_CAMP_LIST_AI where LIST_SEQ_NO = %s;'


