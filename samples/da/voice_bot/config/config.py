#!/usr/bin/python
#-*- coding: utf-8 -*-


class DaConfig:
    campaign_id = 6
    model = 'AI_VOICE_BOT'
    logger_name = 'SIMPLE_BOT'
    log_dir_path = '/home/sun/maum/logs/ai_voice_bot'
    log_file_name = 'ai_voice_bot.log'
    log_backup_count = 3
    log_level = 'debug'
    web_socket_ip = '10.122.64.152'
    web_socket_port = '13254'
    program_id = '11'
    silence_time = '3.0'
    tts_tempo = '1.1'
    tranfer_no = '01063411490'
    simple_eng_tts_tempo ='1.0'


class TomsSdsConfig:
    host = '0.0.0.0'
    port = '9860'
    model = 'TOMS'
    lang = 0
    is_external = True


class BSConfig:
    host = '10.122.64.152'
    port = '6002'
    #port = '5000'


class MysqlVoiceConfig:
    host = '10.122.64.152'
    port = 3306
    user = 'minds'
    passwd = 'msl1234~'
    db = 'HAPPYCALL4'
    charset = 'utf8'


class HCallMysqlConfig:
    host = '10.122.64.152'
    port = 3306
    user = 'minds'
    passwd = 'msl1234~'
    db = 'HAPPYCALL4'
    charset = 'utf8'


class HCallMysqlConfigDev:
    host = '10.122.64.116'
    port = 3306
    user = 'fast'
    passwd = 'Minds12#$'
    db = 'fast_aicc'
    charset = 'utf8'


class SimpleBotMaumSDSConfig:
    srv_address = '13.125.179.153:6941'
    url = 'collect/run/utter'
    host = '196' # ynbot_admin
    lang = '1'
    header1 = 'Content-Type: application/json'
    header2 = 'cache-control: no-cache'


class SimpleBotMaumSDSConfigLocal:
    srv_address = 'localhost:6941'
    url = 'collect/run/utter'
    host = '93' # ynbot_admin
    lang = '1'
    header1 = 'Content-Type: application/json'
    header2 = 'cache-control: no-cache'


class SimpleSocketConfig:
    host = 'https://fast-aicc.maum.ai'
    port = '51000'
